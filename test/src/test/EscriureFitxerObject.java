package test;
import java.io.*;

public class EscriureFitxerObject {
	public EscriureFitxerObject(Cotxe cotxe) throws IOException {
		
		//Declaració del fitxer
		File fitxer = new File("db.txt");
		//Crea el flux de sortida
		FileOutputStream fileout = new FileOutputStream(fitxer);
		//Connectar el flux de bytes al flux de dades
		ObjectOutputStream dataOuComarq = new ObjectOutputStream(fileout);
		dataOuComarq.writeObject(cotxe);//L'escriu al fixer
		dataOuComarq.close();//Tanca el stream de sortida
	}
}
