package test;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class LlegirFitxerObject {
	public LlegirFitxerObject() throws IOException {
   
	    File data = new File("db.txt");
	    System.out.println("test");
		//Camp variable tipus Cotxe
		Cotxe cotxe;
		//Crea el flux d'entrada
		FileInputStream filein = new FileInputStream(data);
		//Connectar el flux de bytes al flux de dades
		ObjectInputStream dataInComarq = new ObjectInputStream(filein);
		
		try {
			while (true){//Llegeix el fitxer
				//Llegeix la comarca
				cotxe = (Cotxe) dataInComarq.readObject();
				System.out.println( "\nMatricula: "+cotxe.getMatricula()+
						"\nMarca: "+cotxe.getMarca()+
						"\nModel: "+cotxe.getModel()+
						"\nAny: "+cotxe.getAny());
			}
		} catch (EOFException eo) {
			System.err.println(eo);
		} catch (Exception e) {
			System.err.println(e);
		}
		dataInComarq.close();//Tanca el stream d'entrada
	}



}
