/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package provajavalibrary1;

import ElsMeusBeans.Comanda;
import ElsMeusBeans.Producte;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/**
 *
 * @author Kitsune
 */
public class ProvaJavaLibrary1 {

    public static int count = 1;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //llistat de productes
        ArrayList<Producte> productes = new ArrayList<Producte>();
        productes.add(new Producte(1, "Portable MSI USB 3.0", 10, 3, 50));
        productes.add(new Producte(2, "HDD WESTERDIGITAL 2TB SATA", 35, 5, 100));
        productes.add(new Producte(3, "Raspberry Pi 4 4GB", 8, 4, 70));

        for (Producte producte : productes) {
            Random rand = new Random();
            int quantitat = rand.nextInt(5) + 5;
            Date data = new Date();
            Comanda comanda = new Comanda(count, producte.getIdproducte(), data, quantitat);
            makeComanda(producte, comanda);
        }

    }

    public static void makeComanda(Producte producte, Comanda comanda) {

        producte.addPropertyChangeListener(comanda);
        //calculem la quantitat actual d'estoc 
        int quantitat = comanda.getQuantitat();
        int stockactual = producte.getStockactual();
        //dades de la comanda
        String info = "\nID Comanda: " + comanda.getNumcomanda()
                + "\nID Producte: " + comanda.getIdproducte()
                + "\nData: " + comanda.getData()
                + "\nQuantitat Sol·licitada: " + comanda.getQuantitat();
        System.out.println(info);
        producte.setStockactual(stockactual - quantitat);
        if (comanda.isDemana()) {
            count++;
            System.out.println("Quantitat en Stock: " + producte.getStockactual());
            System.out.println("Quantitat en Minima Stock per fer comanda: " + producte.getStockminim());
            System.out.println("Fer comanda en producte: " + producte.getDescripcio());
        } else {
            System.out.println("Quantitat en Stock: " + producte.getStockactual());
            System.out.println("Quantitat en Minima Stock per fer comanda: " + producte.getStockminim());
            System.out.println("No és necesari fe la comanda en producte: " + producte.getDescripcio());
        }
    }

}
