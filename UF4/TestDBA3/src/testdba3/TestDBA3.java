/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testdba3;

import ElsMeusBeansA3.Comanda;
import ElsMeusBeansA3.Venda;
import ElsMeusBeansA3.Producte;
import ElsMeusBeansA3.BaseDades;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;
import sun.security.pkcs11.Secmod;

/**
 *
 * @author Kitsune
 */
public class TestDBA3 {

    private static BaseDades bd;
    //Variable de error
    private static Exception e = null;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //MSYQL 
        String host = "jdbc:mysql://localhost:3306/";
        String options = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String table = "m6_uf4a3";
        String usuari = "root";
        String contrasenya = "";
        String driver = "com.mysql.cj.jdbc.Driver";

        String urldb = host + table + options;

        //Es crea un objecte BaseDades
        bd = new BaseDades(urldb, usuari, contrasenya, driver);
        bd.setCreaConnexio();//Es crea la connexio a la base de dades

        if (bd.getCrearConnexio()) {
            /*System.out.println("Connectat");
             System.out.println("========================");
             System.out.println("LLISTA INICIAL DE PRODUCTES");
             VeureProductes(bd);
            
             //Crea una venda
             System.out.println("========================");
             System.out.println("ES CREA VENDA DE ID 3 AMB QUANTITAT 2 ");
             CrearVenda(bd,3,2); //Si no hi ha estoc no es crea venda
            
             System.out.println("========================");
             System.out.println("LLISTA DE PRODUCTES DESPRÉS DE CREAR VENDA ");
             VeureProductes(bd);
            
             System.out.println("========================");
             System.out.println("LLISTA DE VENDES");
             VeureVendes(bd);
            
             System.out.println("========================");
             System.out.println("LLISTA DE COMANDES");
             VeureComandes(bd);*/
            Menu();
        } else {
            System.out.println("NO connectat");
            //tanca connexio
            bd.tancarConnexio();
        }
    }

    //Mostra els productes
    private static void VeureProductes(BaseDades bd) {
        ArrayList<Producte> llista = new ArrayList<>();
        llista = bd.consultaPro("SELECT * FROM PRODUCTE");
        if (llista.size() > 0) {
            for (int i = 0; i < llista.size(); i++) {
                Producte p = (Producte) llista.get(i);
                System.out.println("ID PRODUCTE =>" + p.getIdproducte() + ": " + p.getDescripcio()
                        + "\nEstoc Minim: " + p.getStockminim()
                        + "\nEstoc Actual: " + p.getStockactual()
                        + "\nPVP: " + p.getPvp());
            }
        } else {
            System.out.println("No hi ha productes");
        }
    }

    // Mostra comandes
    private static void VeureComandes(BaseDades bd) {
        ArrayList<Comanda> llista = new ArrayList<>();
        llista = bd.consultaCom("SELECT * FROM COMANDES");
        if (llista.size() > 0) {
            for (int i = 0; i < llista.size(); i++) {
                Comanda c = (Comanda) llista.get(i);
                Producte prod = bd.consultarUnProducte(c.getIdproducte());
                System.out.println("ID COMANDA =>" + c.getNumcomanda()
                        + "\nProducte: " + prod.getDescripcio()
                        + "\nQuantitat: " + c.getQuantitat()
                        + "\nData: " + c.getData());
            }
        } else {
            System.out.println("No hi ha comandes");
        }
    }

    // Mostra vendes
    private static void VeureVendes(BaseDades bd) {
        ArrayList<Venda> llista = new ArrayList<>();
        llista = bd.consultaVen("SELECT * FROM VENDES");
        if (llista.size() > 0) {
            for (int i = 0; i < llista.size(); i++) {
                Venda p = (Venda) llista.get(i);
                Producte prod = bd.consultarUnProducte(p.getIdproducte());
                System.out.println("ID VENDA =>" + p.getNumvenda()
                        + "\nProducte: " + prod.getDescripcio()
                        + "\nQuantitat: " + p.getQuantitat()
                        + "\nData: " + p.getDataventa());
            }
        } else {
            System.out.println("No hi ha ventes");
        }
    }

    //S'insereix una venda
    private static void CrearVenda(BaseDades bd, int idproducte, int quantitat) {
        Producte prod = bd.consultarUnProducte(idproducte);
        java.sql.Date dataActual = getCurrentDate(); //Data SQL
        if (prod != null) {
            if (bd.actualizarStoc(prod, quantitat, dataActual) > 0) {
                //hi ha estoc
                String taula = "VENDES";
                int idvenda = bd.obtenirUltimID(taula);
                Venda ven = new Venda(idvenda, prod.getIdproducte(), dataActual, quantitat);
                if (bd.inserirVenda(ven) > 0) {
                    System.out.println("VENDA INSERIDA....");
                }
            } else {
                System.out.println("NO ES POT FER LA VENDA, NO HI HA ESTOC...");
            }
        } else {
            System.out.println("NO HI HA EL PRODUCTE");
        }

    }

    //S'insereix un prodructe
    private static void CrearProducte(BaseDades bd, Producte prod) {

        if (bd.inserirProducte(prod) > 0) {
            System.out.println("PRODUCTE INSERIT");
        } else {
            System.out.println("NO S'HA INSERIT EL PRODUCTE");
        }
    }

    //Menu
    private static void Menu() {
        //boolean de control loop
        boolean ctrl = true;

        //Missatges
        String msgErrOption = "Opció no contemplada";
        //scaner teclat
        Scanner teclat = new Scanner(System.in);
        //e es una variable statica per controlar les excepcions dintre del loop
        if (e == null) {

            while (ctrl) {
                //boolean de control loop Afegir Producte/Venta
                boolean add = true;
                //text menu
                System.out.println("Introdueix el numero de la opció desitjada"
                        + "\n[0] Sortir"
                        + "\n[1] Afegir nou PRODUCTE"
                        + "\n[2] Afegir nova VENDA"
                        + "\n[3] Veure PRODUCTES"
                        + "\n[4] Veure VENDES"
                        + "\n[5] Veure COMANDES"
                );

                try {
                    int opcio = teclat.nextInt();
                    switch (opcio) {
                        case 0:
                            //Sortir
                            System.out.println("bye, bye ...");
                            teclat.close();
                            ctrl = false;
                            break;
                        case 1:
                            while (add) {
                                //Nou producte
                                System.out.println("Introdueix descripció");
                                teclat.nextLine();
                                String descripcio = teclat.nextLine();
                                System.out.println("Introdueix Stock Actual");
                                int stockactual = teclat.nextInt();
                                System.out.println("Introdueix Stock Minim");
                                int stockminim = teclat.nextInt();
                                System.out.println("Introdueix PVP");
                                float pvp = teclat.nextFloat();
                                //nou id
                                int idproducte = bd.obtenirUltimID("PRODUCTE");
                                Producte prod = new Producte(idproducte, descripcio, stockactual, stockminim, pvp);
                                System.out.println("ID PRODUCTE =>" + prod.getIdproducte() + ": " + prod.getDescripcio()
                                        + "\nEstoc Minim: " + prod.getStockminim()
                                        + "\nEstoc Actual: " + prod.getStockactual()
                                        + "\nPVP: " + prod.getPvp()
                                        + "\n\n ES CORRECTE? [S][N]");
                                String resposta = teclat.next();
                                //cas afirmatiu
                                if (resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("si")) {
                                    CrearProducte(bd, prod);
                                    add = false;
                                    //cas negatiu
                                } else if (resposta.equalsIgnoreCase("N") || resposta.equalsIgnoreCase("no")) {
                                    System.out.println("PRODUCTE NO INTRODUIT");
                                    add = false;
                                } else {
                                    System.out.print(msgErrOption);
                                }
                            }//loop nouProducte
                            break;
                        case 2:
                            //Nova Venda
                            while (add) {
                                //Mostra els productes
                                VeureProductes(bd);
                                System.out.println("Introdueix ID del PRODUCTE");
                                int idproducte = teclat.nextInt();
                              
                                Producte prod = bd.consultarUnProducte(idproducte);
                                //comprova si el producte existeis
                                    if (prod != null) {
                                    System.out.println("Introdueix Quantitat");
                                    int quantitat = teclat.nextInt();
                                    //Mira si hi ha Stock suficient
                                    if (prod.getStockactual() >= quantitat) {

                                        //Mostra les dades dela venta
                                        int idVenda = bd.obtenirUltimID("VENDES");
                                        Venda novaVenda = new Venda(idVenda, idproducte, getCurrentDate(), quantitat);

                                        System.out.println("ID VENDA =>" + novaVenda.getNumvenda()
                                                + "\nProducte: " + prod.getDescripcio()
                                                + "\nQuantitat: " + novaVenda.getQuantitat()
                                                + "\nData: " + novaVenda.getDataventa()+
                                                "\n\n ES CORRECTE? [S][N]"
                                        );
                                        String resposta = teclat.next();
                                        //cas afirmatiu
                                        if (resposta.equalsIgnoreCase("S") || resposta.equalsIgnoreCase("si")) {
                                            CrearVenda(bd, idproducte, quantitat);
                                            add = false;
                                            //cas negatiu
                                        } else if (resposta.equalsIgnoreCase("N") || resposta.equalsIgnoreCase("no")) {
                                            System.out.println("VENDA NO INTRODUIT");
                                            add = false;
                                        } else {
                                            System.out.print(msgErrOption);
                                        }
                                    } else {
                                        System.out.println("No hi ha prou Estock de " + prod.getDescripcio() + "\nEstoc Disponible: " + prod.getStockactual());
                                    }
                                } else {
                                    System.out.println("Producte amb ID " + idproducte + " : No Existeix");
                                }
                            }//loop nouProducte
                            break;
                        case 3:
                            //Veure Productes
                            System.out.println("Productes:");
                            VeureProductes(bd);
                            break;
                        case 4:
                            //Veure Ventes   
                            System.out.println("Vendes:");
                            VeureVendes(bd);
                            break;
                        case 5:
                            //Veure Comandes
                            System.out.println("Comandes:");
                            VeureComandes(bd);
                            break;
                        default:
                            System.out.println(msgErrOption);
                            break;
                    }
                } catch (InputMismatchException errorInput) {
                    //reiniciar el teclat en cas d'error
                    teclat.reset();
                    ctrl = false;
                    e = new Exception("FORMAT DE LES DADES INCORRECTE", errorInput);
                }
            }
            //end while
        }
        //end if e==null;
        if (e != null) {
            //mosterm el missatge d'error
            System.out.println(e.getMessage());
            //reiniciem la variable d'error
            e = null;
            //recargem el menu
            Menu();
        }
    }

    //obte la data actual
    private static java.sql.Date getCurrentDate() {
        java.util.Date avui = new java.util.Date();
        return new java.sql.Date(avui.getTime());
    }

}
