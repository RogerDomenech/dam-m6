package test;

import java.io.Serializable;

public class Comarca implements Serializable {
					//Implementa la interf�cie Serializable
	private String nom;
	private int habitants;
	
	//constructor amb par�metres
	public Comarca (String nom, int habitants){
		//per no confondre el par�metre amb el camp de variable
		this.nom = nom;
		//per no confondre el par�metre amb el camp de variable
		this.habitants = habitants;
	}
	public Comarca (){//constructor per defecte
		this.nom = null;
	}
	//per donar el valor als camps de variable private
	public void setNom(String comarca){nom = comarca;}
	public void setHabitants(int poblacio){ habitants = poblacio;}
	//per consultar el valor dels camps de variable private
	public String getNom(){return nom;}
	public int getHabitants(){return habitants;}

}
