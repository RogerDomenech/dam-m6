package test;
import java.io.*;

public class EscriureFitxerObject {
	public static void main(String[] args) throws IOException {
		//Camp variable tipus Comarca
		Comarca comarca;
		//Declaraci� del fitxer
		File fitxer = new File("comarquesObject.txt");
		//Crea el flux de sortida
		FileOutputStream fileout = new FileOutputStream(fitxer);
		//Connectar el flux de bytes al flux de dades
		ObjectOutputStream dataOuComarq = new ObjectOutputStream(fileout);
		//Les dades per generar els objectes Comarca
		String comarq[] = {"Baix Camp", "Segarra", "Bages", "Priorat", "Terra Alta", "Montsi�", "Alt Camp", "Anoia", 					"Maresme"};
		int poblacio[] = {190249, 22713, 184403, 9550, 12119, 69613, 44578, 117842, 437919};
		//Recorre els arrays
		for (int i=0; i<comarq.length; i++){//Crea la comarca
			comarca = new Comarca(comarq[i], poblacio[i]);
			dataOuComarq.writeObject(comarca);//L'escriu al fixer
		}
		dataOuComarq.close();//Tanca el stream de sortida
	}
}
