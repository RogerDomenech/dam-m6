package test;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class LlegirFitxerObject {
   public static void main(String[] args) throws IOException, ClassNotFoundException {
		//Camp variable tipus Comarca
		Comarca comarca;
		//Declaraci� del fitxer
		File fitxer = new File("comarquesObject.txt");
		//Crea el flux d'entrada
		FileInputStream filein = new FileInputStream(fitxer);
		//Connectar el flux de bytes al flux de dades
		ObjectInputStream dataInComarq = new ObjectInputStream(filein);
		
		try {
			while (true){//Llegeix el fitxer
				//Llegeix la comarca
				comarca = (Comarca) dataInComarq.readObject();
				System.out.println("Comarca: " +comarca.getNom()+ 
				"Poblaci�: "+ comarca.getHabitants()+"  habitants");
			}
		} catch (EOFException eo) {}
		dataInComarq.close();//Tanca el stream d'entrada
	}



}
