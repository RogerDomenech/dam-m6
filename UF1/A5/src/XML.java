import java.awt.image.CropImageFilter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XML {
	//propietats
	private String primarykey;
	private File file;
	private Document document;
	private Element root;
	private ArrayList<String>fields;
	static boolean control=false;
	//constructor
	public XML(String primarykey, String file) {
		this.primarykey = primarykey;
		this.file = new File(file); 
		try {
			this.root = this.root();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.fields=new ArrayList<String>();
		this.read(root);
		//this.fields = this.readfields(this.doc);
	}
	//metodes
	public ArrayList<String> readfields(Node document) {
		//container
		ArrayList<String>fields= new ArrayList<String>();
		boolean is_exist = fields.contains(document.getNodeName());
		if(!is_exist) {
			fields.add(document.getNodeName());
		}
		
		// mirem si hi nhan fills 
		if(document.hasChildNodes()) {
			// recorrem els fills
			NodeList childs = root.getChildNodes();
			for (int i = 0; i < childs.getLength(); i++) {
				Node child = childs.item(i);
				is_exist = fields.contains(child.getNodeName());
				if(!is_exist) {
					fields.add(child.getNodeName());
				}
				if(child.hasChildNodes()) {
					this.readfields(child);
				}
			}
			//acaba de llegir les fills
		}
		// si te germans
		if(document.getNextSibling()!=null) {
			Node bro = document.getNextSibling();
			this.readfields(bro);
		}
		return fields;
	}
	public void add() {

	}
	public void index(String primarykey) {
		/// element.setIdAtrribute("id",true)
	}
	public Element root() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(this.file);
		Element nodeArrel = doc.getDocumentElement();
		this.document=doc;
		return nodeArrel;
	}

	public String read(Node elements) {
		String nameNode = elements.getNodeName();
		String valueNode = elements.getNodeValue();
		String prevSibling = (elements.getPreviousSibling()!=null)?elements.getPreviousSibling().getNodeName():"null";
		String postSibling = (elements.getNextSibling()!=null)?elements.getNextSibling().getNodeName():"null";
		String space="\t";
		String document ="";
		if(!nameNode.equals("#text")) {
			document+="<"+nameNode+">";
			Element pk = this.document.getElementById(primarykey);
			//1
			/*if(pk.getNodeName().equals(nameNode)) {
				control=(control)?false:true;
			}*/
			boolean is_exist = this.fields.contains(nameNode);
			if(!is_exist&&control) {
				this.fields.add(nameNode);
			}
		}
		try {
			if(!valueNode.equals(null)||!valueNode.equals(" ")){
				document+=valueNode;
			}

			if(postSibling=="null") {
				document+="</"+elements.getParentNode().getNodeName()+">";

				/*if(elements.getParentNode().getNextSibling()!=null) {
					System.out.print("test "+count);
				}*/
			}

			//System.out.print(elements.getParentNode().getNextSibling());
		} catch (NullPointerException e) {

		}
		//System.out.println(space);
		//si te fills
		if(elements.hasChildNodes()) {
			// mira la llergada 
			int size =elements.getChildNodes().getLength();
			// recorre
			for (int i = 0; i < size; i++) {
				Node element= elements.getChildNodes().item(i);
				read(element);
			}
		}
		return document;
	}

	public void save(String outFile){
		try {
			//iniciar transformer
			Transformer itsAtrap = TransformerFactory.newInstance().newTransformer();
			//creacio el adapador soures i resul aprtri del doc i file
			File exit = new File(outFile);
			StreamResult result = new StreamResult(exit);
			DOMSource source = new DOMSource(root);
			itsAtrap.transform(source, result);

		} catch (TransformerFactoryConfigurationError | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//gets && sets
	public String getPrimarykey() {
		return primarykey;
	}
	public void setPrimarykey(String primarykey) {
		this.primarykey = primarykey;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public Element getDoc() {
		return root;
	}
	public void setDoc(Element doc) {
		this.root = doc;
	}
	public ArrayList<String> getFields() {
		return fields;
	}
	public void setFields(ArrayList<String> fields) {
		this.fields = fields;
	}


}
