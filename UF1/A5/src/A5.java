import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class A5 {

	static int id=0;
	static Document doc;
	static String pk="id";
	static Element nodeArrel;
	static File exit= new File("exit.xml");
	static Element found;
	static boolean ctrl=true;
	static String nomAlumne;
	static String cognom1Alumne;
	static String cognom2Alumne;
	static int notaAlumne;
	static int idAlumne;
	
	public static void main(String[] args) throws Exception {
		File file =new File(args[0]);
	
		//File file = new File("alumnes.xml");
		//File file = new File("yf2b-mjr6.xml");
		//File file= new File("residus.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(file);
		nodeArrel = doc.getDocumentElement();
		Scanner teclat = new Scanner(System.in);
		while(ctrl){
			System.out.println("Menu:\n "+
					"\n 0-Sortir"+
					"\n 1-LLegir fitxer "+file.getName()+
					"\n 2-Crear nou Alumne"+
					"\n 3-Modificar Alumne"+
					"\n 4-Borrar Alumne"+
					"\n 5-Guardar en fitxer "+exit.getName());
			int option= teclat.nextInt();
			if(option>=0 && option<=5) {	
				switch (option) {
				case 0:
					//sortir
					ctrl=false;
					System.out.println("byebye..");
					break;
				case 1:
					//llegir 
					System.out.println(nodeToString(nodeArrel));
					break;
				case 2:
					//crear
					System.out.println("Introdueix nom");
					nomAlumne = teclat.next();
					System.out.println("Introdueix 1er Cognom");
					cognom1Alumne = teclat.next();
					System.out.println("Introdueix 2on Cognom");
					cognom2Alumne = teclat.next();
					System.out.println("Introdueix Nota");
					notaAlumne = teclat.nextInt();
					Element alumne = newAlumne(nomAlumne, cognom1Alumne, cognom2Alumne, notaAlumne);
					nodeArrel.appendChild(alumne);
					System.out.println(nodeToString(nodeArrel));
					break;
				case 3:
					//modificar
					System.out.println("Introdueix Identificador");
					idAlumne = teclat.nextInt();
					System.out.println("Introdueix nom");
					nomAlumne = teclat.next();
					System.out.println("Introdueix 1er Cognom");
					cognom1Alumne = teclat.next();
					System.out.println("Introdueix 2on Cognom");
					cognom2Alumne = teclat.next();
					System.out.println("Introdueix Nota");
					notaAlumne = teclat.nextInt();
					editAlumne(idAlumne, nomAlumne, cognom1Alumne, cognom2Alumne, notaAlumne);
					break;
				case 4:
					//borrar
					System.out.println("Introdueix Identificador");
					idAlumne = teclat.nextInt();
					remove(idAlumne);
					break;
				case 5:
					//guardar en xml
					save();
					System.out.println("Fitxer "+exit.getName()+" actualizat");
					break;
				default:
					break;
				}
			}else {
				System.out.println(option+" Opci� no valida");
			}
			
		}
		teclat.close();
	}

	/**
	 * Funcio de de editar alumne
	 * @param find_id int identificador del alumne
	 * @param nom String nom del alumne
	 * @param cognom1 String 1er Cognom del alumne
	 * @param cognom2 String 2on Cognom del alumne
	 * @param notaFinal int Nota del alumne
	 */
	public static void editAlumne(int find_id,String nom,String cognom1,String cognom2,int notaFinal){
		Element find_element = find(find_id, nodeArrel);
		//buidem el contigut del fill
		while(find_element.hasChildNodes()) {
			find_element.removeChild(find_element.getFirstChild());
		}
		//afegim el nou fill modificat
		Element name = create("nom",nom,"","");
		find_element.appendChild(name);
		Element fristSubname = create("cognom1",cognom1,"","");
		find_element.appendChild(fristSubname);
		Element lastname = create("cognom2",cognom2,"","");
		find_element.appendChild(lastname);
		Element notafinal = create("notaFinal",""+notaFinal,"","");
		find_element.appendChild(notafinal);

	}

	/**
	 * Funcio per borrar alumne 
	 * @param find_id int identificador del alumne
	 * @return Element sense el node trobat
	 */
	public static Element remove(int find_id) {
		Element search = find(find_id, nodeArrel);
		search.getParentNode().removeChild(search);
		return nodeArrel;
	}


	/**
	 * Funcio per guardar en format xml
	 */
	public static void save() {
		try {
			//iniciar transformer
			Transformer itsAtrap = TransformerFactory.newInstance().newTransformer();
			//creacio el adapador soures i resul aprtri del doc i file
			StreamResult result = new StreamResult(exit);
			DOMSource source = new DOMSource(doc);
			itsAtrap.transform(source, result);

		} catch (TransformerFactoryConfigurationError | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * funcio per crear nodes
	 * @param name String nom del node
	 * @param value String contigut del node
	 * @param nameAtt String nom del Attribut
	 * @param valueAtt String valor del Attribut
	 * @return Element formatat
	 */
	public static Element create(String name, String value, String nameAtt ,String valueAtt) {
		Element newElement= doc.createElement(name);
		newElement.setTextContent(value);

		if(nameAtt.length()>=1&&valueAtt.length()>=1) {
			newElement.setAttribute(nameAtt, valueAtt);	
		}
		return newElement;
	}


	/**
	 * Funcio per crear nou alumne
	 * @param nom String nom del alumne
	 * @param cognom1 String 1er cognom del alumne
	 * @param cognom2 String 2on cognom del alumne
	 * @param notaFinal int nota del alumne
	 * @return Element formatat
	 */
	public static Element newAlumne(String nom,String cognom1,String cognom2,int notaFinal) {

		Element alumne = create("alumne", "", "id", ""+maxId(nodeArrel));
		Element name = create("nom",nom,"","");
		alumne.appendChild(name);
		Element fristSubname = create("cognom1",cognom1,"","");
		alumne.appendChild(fristSubname);
		Element lastname = create("cognom2",cognom2,"","");
		alumne.appendChild(lastname);
		Element notafinal = create("notaFinal",""+notaFinal,"","");
		alumne.appendChild(notafinal);

		return alumne;
	}

	/**
	 * Busca un element per id dintre del del node passat
	 * @param find_id int identificador numeric a buscar
	 * @param nodes Node contingut 
	 * @return Element trobat
	 */
	public static Element find(int find_id,Node nodes) {

		if(((Element) nodes).hasAttribute(pk)) {
			int id_actual=Integer.parseInt(((Element) nodes).getAttributeNode(pk).getValue());
			if(id_actual==find_id) {
				found= (Element) nodes;
			}
		}
		if(nodes.hasChildNodes()) {
			//length dels fills
			int length = nodes.getChildNodes().getLength();

			for (int i = 0; i < length; i++) {
				NodeList childs = nodes.getChildNodes();

				Node child = childs.item(i);
				if(child instanceof Element) {
					find(find_id,(Element) child);
				}
			}
		}

		return found;
	}

	/**
	 * Busca el ultim id i el incrementa 1 
	 * @param nodes Node on buscar el id
	 * @return int retorna el idMax + 1 
	 */
	public static int maxId(Node nodes){

		if(((Element) nodes).hasAttribute(pk)) {
			int id_actual=Integer.parseInt(((Element) nodes).getAttributeNode(pk).getValue());
			id=(id_actual>id)?id_actual+1:id;
		}
		if(nodes.hasChildNodes()) {
			//length dels fills
			int length = nodes.getChildNodes().getLength();

			for (int i = 0; i < length; i++) {
				NodeList childs = nodes.getChildNodes();

				Node child = childs.item(i);
				if(child instanceof Element) {
					maxId((Element) child);
				}
			}
		}

		return id;
	}

	/**
	 * Passa de de node a string  
	 * @param node Node arecorrer
	 * @return String amb el node
	 * @throws Exception
	 */
	private static String nodeToString(Node node) throws Exception{
		StringWriter sw = new StringWriter();

		Transformer t = TransformerFactory.newInstance().newTransformer();
		t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		t.setOutputProperty(OutputKeys.INDENT, "yes");
		t.transform(new DOMSource(node), new StreamResult(sw));

		return sw.toString();
	}
}
