import java.io.*;


import javax.xml.parsers.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/*
 Has de codificar un programa que llegeixi un arxiu xml i mostri per pantalla tots 
 els nodes que cont� el document i, per a cada node, ha de mostrar els seus atributs
  (i valors) si en t� i per a cada node fill, els seus atributs (i valors) si en t� i,
   per a cada node fill, ....(el mateix, haureu d�usar una funci�/m�tode recursiva)

De cada node volem veure el tipus, i el nom. De cada atribut el nom i el valor.

Les instruccions a usar son:

// per a carregar en mem�ria un arxiu xml
File file = new File("arxiu.xml");
DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
doc = dBuilder.parse(file);

//per obtenir el node arrel
Element nodeArrel = doc.getDocumentElement();

//Per obtenir els nodes fill d�un node useu el m�tode getChildNodes()
//Per obtenir els atributs d�un node, useu el m�tode getAttributes()
//Per obtenir el nom, el tipus i el valor d�un node, cerqueu els m�todes apropiats.
 */

public class A4 {
	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
		//File file = new File("alumnes.xml");
		//File file = new File("yf2b-mjr6.xml");
		File file= new File(args[0]);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		Element nodeArrel = doc.getDocumentElement();
		NodeList fills = nodeArrel.getChildNodes();
		read(nodeArrel);

	}
	static int numSpais = 0;
	static String headNode="";
	static String contentNode="";
	static String endNode ="";
	static String nomNode="";
	static String print ="";
	public static void read(Node elements) {
		String nameNode = elements.getNodeName();

		if(isTextNode(elements)) {
			nameNode="";

			if(!elements.getNodeValue().equals(null)) {
				contentNode=elements.getNodeValue();
				String prevSibling = (elements.getPreviousSibling()!=null)?elements.getPreviousSibling().getNodeName():"null";
				String postSibling = (elements.getNextSibling()!=null)?elements.getNextSibling().getNodeName():"null";
				if(!elements.hasChildNodes()&& prevSibling=="null" && postSibling=="null") {
					endNode="</"+elements.getParentNode().getNodeName()+">";

				}

				if(prevSibling!="null" && postSibling=="null" ) {
					endNode = print+"</"+elements.getPreviousSibling().getParentNode().getNodeName()+">";
				}
			}
			numSpais++;
		}else {
			NamedNodeMap attributeNode = elements.getAttributes();
			String valueNode = elements.getNodeValue();

			String attr="";
			// espais a generarar
			for (int i = 0; i < numSpais; i++) {
				print+=" ";
			}
			// atributs a colocar
			for (int i = 0; i < attributeNode.getLength(); i++) {
				attr+=attributeNode.item(i).getNodeName()+"="+attributeNode.item(i).getNodeValue()+" ";
			}
			//controla el espai de debant dels atributs
			attr=(attr.length()<1)?"":" "+attr;
			nomNode = nameNode;
			headNode = print+"<"+nomNode+attr+">";
			numSpais=0;
		}
		//si te fills
		if(elements.hasChildNodes()) {
			// mira la llergada 
			int size =elements.getChildNodes().getLength();
			// recorre
			for (int i = 0; i < size; i++) {
				Node element= elements.getChildNodes().item(i);
				read(element);
			}
		}else {

			System.out.println(headNode+contentNode+endNode);
			headNode="";
			contentNode="";
			endNode="";
			print="";
		}

	}

	static boolean isTextNode(Node node) {
		return node.getNodeName().equalsIgnoreCase("#text");
	}
	static boolean isLastChildNode(Node node) {
		return node.getParentNode().getLastChild().getNodeName().equalsIgnoreCase(node.getNodeName());
	}
	static String clear(String string) {
		string.trim();
		String pattern = "\n|\t";
		return string.replaceAll(pattern, "");
	}
}
