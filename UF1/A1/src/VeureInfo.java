
import java.io.*;

/*
Accepti de l�nia d'ordres (args[0]) el directori o fitxer que es vol consultar. 
Cal comprova que existeix file.exists()
Si �s un directori (file.isDirectory()) que mostri el seu contingut, com ho fa a CODI1.1
Si �s un fitxer (file.isFile()) que mostri la seva informaci�, com ho fa a CODI1.2
 */
/**
 *
 * @author Roger Domenech
 */
public class VeureInfo {

	public static void main(String[] args) {
		try {
			//directori del fitxer
			String input = args[0];
			//crea file 
			File f = new File(input);
			if (f.exists()) {
				// si es directori
				if (f.isDirectory()) {
					String[] arxius = f.list();
					for (int i = 0; i < arxius.length; i++) {
						File thisFile = new File(input+"/"+arxius[i]);
						if(thisFile.isHidden()){
							System.out.println(arxius[i]+"[hidden]");
						}else{
							System.out.println(arxius[i]);
						}
					}
					// si es arxiu    
				} else {
					String ocult = (f.isHidden())?"Si":"No";
					System.out.println("Fitxer ocult : "+ ocult);
					System.out.println("Nom del fitxer : " + f.getName());
					System.out.println("Ruta           : " + f.getPath());
					System.out.println("Ruta absoluta  : " + f.getAbsolutePath());
					System.out.println("Es pot escriure: " + f.canRead());
					System.out.println("Es pot llegir  : " + f.canWrite());
					System.out.println("Grandaria      : " + f.length());
					System.out.println("Es un directori: " + f.isDirectory());
					System.out.println("Es un fitxer   : " + f.isFile());

				}
			} else {
				System.out.println("Fitxer / directori No exiteixs");

			}

		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("\nNo has introduit Fitxer / directori");
		}catch (Exception e) {
			// TODO: handle exception
		}

	}
}
