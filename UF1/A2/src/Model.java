import java.io.*;
import java.util.ArrayList;
import java.util.List;

/*
 Pugui guardar i recuperar objectes tipus Cotxe com un bloc.
 Pugui guardar objectes tipus Cotxe d'un en un.
 Pugui recuperar objectes tipus Cotxe segons un camp.
 */
/**
 *
 * @author Roger Domenech
 */
public class Model implements Serializable{

    static String url;
    static File data;

    //constructor
    public Model(String url) throws IOException {
        this.url = url;
        this.data = new File(url);
        if(!data.exists()) {
        	data.createNewFile();
        }
    }

    public Model() {
    }

    //Method


    public String read() throws IOException {
    	String request="";
    	//Crea el flux d'entrada
    	if(data.length()!=0) {
        FileInputStream filein = new FileInputStream(data);
        //Connectar el flux de bytes al flux de dades
        ObjectInputStream dataIn = new ObjectInputStream(filein);
        request ="El fitxer "+data.getName()+" esta preparat";
        dataIn.close();
    	}else {
    		request="El fitxer "+data.getName()+" esta buit";
    	}
        return request;
    }
    
    //Gets & Sets
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public File getData() {
        return data;
    }

    public void setData(File data) {
        this.data = data;
    }

}
