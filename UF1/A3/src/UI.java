import java.io.IOException;
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Roger Domenech
 */
public class UI {
	public static void main(String[]args) throws IOException, ClassNotFoundException {
		// conexio al model
		Model model= new Model("db.txt");
		//System.out.println(model.read());
		// control bucle
		boolean ctrl = true;
		//Cotxe newCotxe = new Cotxe();
		Scanner teclat = new Scanner(System.in);
		while(ctrl) {
			System.out.println("\nSeleciona opcio:"+
		                       "\n0-Sortir"+
					           "\n1-Nova Fitxa"+
		                       "\n2-Mostra Totes Fitxes"+
					           "\n3-Buscar Fitxa");
			int input = teclat.nextInt();
			switch(input){
			case 0:
				System.out.println("Sortint...");
				ctrl=false;
				break;
			case 1:
				//recollida de dades
				ArrayList<String>inputs= new ArrayList<String>();
				String[] fields = new FitxaPersonal().getFields();
				for (String field : fields) {
					System.out.println("Introdueix "+field);
					inputs.add(teclat.next());
				}
				
				//Montem model
				FitxaPersonal newFitxa = new FitxaPersonal(inputs.get(0),inputs.get(1),inputs.get(2),inputs.get(3),inputs.get(4));
				System.out.println(newFitxa.toString());
				//guardem
				newFitxa.save();
				break;
			case 2:
				//consultem totes les fitxes
				ArrayList<FitxaPersonal>allFitxes =new FitxaPersonal().all();
				int count =1;
				//printem
				for (FitxaPersonal fitxa : allFitxes) {
					System.out.println("Fitxa num: "+count+"\n"+fitxa.toString());
					count++;
				}
				break;
			case 3:
				System.out.println("Introdueix el id a buscar");
				int search = teclat.nextInt();
				ArrayList<FitxaPersonal> find = new FitxaPersonal().find(search);
				for (FitxaPersonal fitxaPersonal : find) {
					System.out.println(fitxaPersonal.toString());
				}
				break;
			default:
				break;
			}
		}
			teclat.close();
		
	};
	
}
