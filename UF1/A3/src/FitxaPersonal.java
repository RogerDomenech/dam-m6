import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FitxaPersonal extends Model{
	//propietats
	
	private String nom;
	private String cognom1;
	private String cognom2;
	private String telefon;
	private String email;
	private String[] fields = {"Nom","1er Cognom","2on Cognom","Telefon","Email"};
	private Integer[] fieldSize = {10,20,20,10,20};

	//constructor
	public FitxaPersonal(String nom, String cognom1, String cognom2, String telefon, String email) {
		super();
		this.nom = nom;
		this.cognom1 = cognom1;
		this.cognom2 = cognom2;
		this.telefon = telefon;
		this.email = email;
	}

	public FitxaPersonal() {
		super();
	}

	//gets & sets
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognom1() {
		return cognom1;
	}
	public void setCognom1(String cognom1) {
		this.cognom1 = cognom1;
	}
	public String getCognom2() {
		return cognom2;
	}
	public void setCognom2(String cognom2) {
		this.cognom2 = cognom2;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	//metodes
	public ArrayList<FitxaPersonal> all() throws IOException, ClassNotFoundException{
		//container
		ArrayList<FitxaPersonal> container = new ArrayList<FitxaPersonal>();
		
		//Crea un flux (stream) d'arxiu d'acc�s aleatori nom�s lectura
		RandomAccessFile aleatoriFile = new RandomAccessFile(data, "r");

		//Apuntador s'inicialitza apuntant a l'inici del fitxer
		//possicions dels elements
		int apuntador = 0;
		int posNom;
		int posCognom1;
		int posCognom2;
		int posTelefon;
		int postEmail;
		//arrays que contenen els caracters
		char chaNom[] = new char[fieldSize[0]];
		char chaCognom1[] = new char[fieldSize[1]];
		char chaCognom2[] = new char[fieldSize[2]];
		char chaTelefon[] = new char[fieldSize[3]];
		char chaEmail[] = new char[fieldSize[4]];
		//lector
		char aux;
		//Recorrer el fitxer llibres
		try {
		for (;;) {
			aleatoriFile.seek(apuntador);//Apuntar a l'inici de cada llibre al fitxer
			//Llegeix Nom
			//posNom = aleatoriFile.readInt();
			for (int i = 0; i < chaNom.length; i++) {
				aux = aleatoriFile.readChar();
				chaNom[i] = aux;
			}
			String nom = new String(chaNom);
			//Llegeix Cognom1
			//posCognom1 = aleatoriFile.readInt();
			for (int i = 0; i < chaCognom1.length; i++) {
				aux = aleatoriFile.readChar();
				chaCognom1[i] = aux;
			}
			String cognom1 = new String(chaCognom1);
			//Llegeix Cognom2
			//posCognom2 = aleatoriFile.readInt();
			for (int i = 0; i < chaCognom2.length; i++) {
				aux = aleatoriFile.readChar();
				chaCognom2[i] = aux;
			}
			String cognom2 = new String(chaCognom1);
			//Llegeix Telefon
			//posTelefon = aleatoriFile.readInt();
			for (int i = 0; i < chaTelefon.length; i++) {
				aux = aleatoriFile.readChar();
				chaTelefon[i] = aux;
			}
			String telefon = new String(chaTelefon);
			//Llegeix Email
			//postEmail = aleatoriFile.readInt();
			for (int i = 0; i < chaEmail.length; i++) {
				aux = aleatoriFile.readChar();
				chaEmail[i] = aux;
			}
			String email = new String(chaEmail);
			//Sortida de les dades de cada llibre
			FitxaPersonal newFitxa = new FitxaPersonal(nom, cognom1, cognom2, telefon, email);
			container.add(newFitxa);
			//System.out.println(newFitxa.toString());
			/*System.out.println("\n"+fields[0]+" "+nom+
					"\n"+fields[1]+" "+cognom1+
					"\n"+fields[2]+" "+cognom2+
					"\n"+fields[3]+" "+telefon+
					"\n"+fields[4]+" "+email);*/
			//S'ha de posicionar l'apuntador al seg�ent llibre
			int totalbyte=0;
			for (int size: fieldSize) {
				totalbyte+=(size*2);
			}
			apuntador += totalbyte;
			//Si coincideix on s'est� apuntat amb el final del fitxer, sortim
			if(aleatoriFile.getFilePointer()==aleatoriFile.length()) break;
		}
		aleatoriFile.close();//Tancar el fitxer
		}catch (Exception e) {
			// TODO: handle exception
		}
	return container;
}

public void save(){

	try {
		//recuperem el contigut del fitxer
		//i el guardem en un arraylist
		ArrayList<FitxaPersonal> allFitxa = this.all();
		allFitxa.add(this);
		//Crea un flux (stream) d'arxiu d'acc�s aleatori per llegir
		RandomAccessFile aleatoriFile = new RandomAccessFile(data, "rw");
		//Construeix un buffer (mem�ria interm�dia) de strings
		StringBuffer buffer = null;
		for (FitxaPersonal fitxaPersonal : allFitxa) {
			
		//Les dades per inserir
		Map<String, String> insert = fitxaPersonal.toMap();

		
		//for (int i = 0; i < insert.size(); i++) {
			//aleatoriFile.writeInt(i+1);//1 enter ocupa 4 bytes
			//Nom
			buffer = new StringBuffer (insert.get(fields[0]));
			buffer.setLength(fieldSize[0]);
			aleatoriFile.writeChars(buffer.toString());
			//1er Cognom
			buffer = new StringBuffer (insert.get(fields[1]));
			buffer.setLength(fieldSize[1]);
			aleatoriFile.writeChars(buffer.toString());
			//2on Cognom
			buffer = new StringBuffer (insert.get(fields[2]));
			buffer.setLength(fieldSize[2]);
			aleatoriFile.writeChars(buffer.toString());
			//Telefon
			buffer = new StringBuffer (insert.get(fields[3]));
			buffer.setLength(fieldSize[3]);
			aleatoriFile.writeChars(buffer.toString());
			//Email
			buffer = new StringBuffer (insert.get(fields[4]));
			buffer.setLength(fieldSize[4]);
			aleatoriFile.writeChars(buffer.toString());

		}
		
		aleatoriFile.close();

	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
public String toString() {
	String request ="\n";
	for (int i = 0; i < fields.length; i++) {
		request+=fields[i];
		switch (i) {
		case 0:
			request+=" : "+this.getNom();
			break;
		case 1:
			request+=" : "+this.getCognom1();
			break;
		case 2:
			request+=" : "+this.getCognom2();
			break;
		case 3:
			request+=" : "+this.getTelefon();
			break;
		case 4:
			request+=" : "+this.getEmail();
			break;

		default:
			break;
		}
		request += "\n";
	}
	return request;
}

public Map<String,String> toMap() {
	Map<String,String> request = new HashMap<String, String>();
	request.put(fields[0],this.getNom());
	request.put(fields[1],this.getCognom1());
	request.put(fields[2],this.getCognom2());
	request.put(fields[3],this.getEmail());
	request.put(fields[4],this.getTelefon());
	return request;
}

public ArrayList<FitxaPersonal> find(int seleccio) throws IOException {
	//container
	ArrayList<FitxaPersonal> container = new ArrayList<FitxaPersonal>();
		
	//Crea un flux (stream) d'arxiu d'acc�s aleatori nom�s lectura
	RandomAccessFile aleatoriFile = new RandomAccessFile(data, "r");
	
	//Apuntador s'inicialitza apuntant a l'inici del fitxer
			//possicions dels elements
			int apuntador = 0;
			int posNom;
			int posCognom1;
			int posCognom2;
			int posTelefon;
			int postEmail;
			//arrays que contenen els caracters
			char chaNom[] = new char[fieldSize[0]];
			char chaCognom1[] = new char[fieldSize[1]];
			char chaCognom2[] = new char[fieldSize[2]];
			char chaTelefon[] = new char[fieldSize[3]];
			char chaEmail[] = new char[fieldSize[4]];
	
			//lector
			char aux;
	int totalbyte=0;
	for (int size: fieldSize) {
		totalbyte+=(size*2);
	}
	
	apuntador = (seleccio-1)*totalbyte;
	
	if (apuntador >= aleatoriFile.length()) {
		System.out.println("ERROR: ID incorrecte, no existeix aquesta fitxa");
	} else {//Apuntar a l'inici del llibre seleccionat al fitxer
		aleatoriFile.seek(apuntador);
		//llegix el nom 
		for (int i = 0; i < chaNom.length; i++) {
			aux = aleatoriFile.readChar();
			chaNom[i] = aux;
		}
		String nom = new String(chaNom);
		//Llegeix Cognom1
		//posCognom1 = aleatoriFile.readInt();
		for (int i = 0; i < chaCognom1.length; i++) {
			aux = aleatoriFile.readChar();
			chaCognom1[i] = aux;
		}
		String cognom1 = new String(chaCognom1);
		//Llegeix Cognom2
		//posCognom2 = aleatoriFile.readInt();
		for (int i = 0; i < chaCognom2.length; i++) {
			aux = aleatoriFile.readChar();
			chaCognom2[i] = aux;
		}
		String cognom2 = new String(chaCognom1);
		//Llegeix Telefon
		//posTelefon = aleatoriFile.readInt();
		for (int i = 0; i < chaTelefon.length; i++) {
			aux = aleatoriFile.readChar();
			chaTelefon[i] = aux;
		}
		String telefon = new String(chaTelefon);
		//Llegeix Email
		//postEmail = aleatoriFile.readInt();
		for (int i = 0; i < chaEmail.length; i++) {
			aux = aleatoriFile.readChar();
			chaEmail[i] = aux;
		}
		String email = new String(chaEmail);
		//Sortida de les dades de cada llibre
		FitxaPersonal newFitxa = new FitxaPersonal(nom, cognom1, cognom2, telefon, email);
		container.add(newFitxa);
	}
	aleatoriFile.close();//Tancar el fitxer
	return container;
}

}


