/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.xmldb.api.base.*;
import org.xmldb.api.modules.*;
import org.xmldb.api.*;

/**
 *
 * @author Kitsune
 */
public class A3 {

    public static XPathQueryService servei;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws XMLDBException {
        //Driver per a exist
        String driver = "org.exist.xmldb.DatabaseImpl";
        //Col·lecció
        Collection col = null;
        //uri col·leccio
        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/proves";
        //Usuari
        String user = "admin";
        //Contraseña
        String pass = "";
        try {
            //carrega el driver
            Class cl = Class.forName(driver);
            //Instancia de la DB
            Database database = (Database) cl.newInstance();
            //Registre del Drive
            DatabaseManager.registerDatabase(database);

        } catch (Exception e) {
            System.out.print("Error en iniciar la base de dades eXist");
            e.printStackTrace();
        }
        col = DatabaseManager.getCollection(URI, user, pass);
        if (col == null) {
            System.out.println("*** LA COL·LECCIÓ NO EXISTEIX ***");
        }
        servei = (XPathQueryService) col.getService("XPathQueryService", "1.0");
        String input = "";
        //menu
        while (!(input.equalsIgnoreCase("Error en llegir") || input.equalsIgnoreCase("0"))) {
            System.out.print("Menu:\n0-Exit\n1-Mostrar Departament\n2-Afegir Departament\n3-Modificar Departament\n4-Eliminar departament\n");
            input = input();
            switch (input) {
                case "0":
                    System.out.println("bye bye ...");
                    break;
                case "1":
                    System.out.print("Introdueix ID del departament: ");
                    int idShow = Integer.parseInt(input());
                    showDept(idShow);
                    break;
                case "2":
                    insereixdep(newDep());
                    break;
                case "3":
                    System.out.print("Introdueix ID del departament: ");
                    int idEdit = Integer.parseInt(input());
                    showDept(idEdit);
                    modificadep(editDep(idEdit));
                    break;
                case "4":
                    System.out.print("Introdueix ID del departament: ");
                    int idDelete = Integer.parseInt(input());
                    showDept(idDelete);
                    esborradep(idDelete);
                    System.out.println("Departament Esborrat");
                    break;
                default:
                    input="Error en llegir";
                    break;
            }
        }
        //S'esborra
        col.close();
    }
    //input
    public static String input() {
        String s = null;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            s = in.readLine();
        } catch (IOException e) {
            System.out.println("Error en llegir");
            e.printStackTrace();
        }
        return s;
    }
    //crear
    public static HashMap<String, String> newDep() {
        HashMap<String, String> newDep = new HashMap<String, String>();
        //numero de departament
        System.out.print("Introdueix numero del Departament: ");
        newDep.put("DEPT_NO", input());
        System.out.println();
        //nom del departament
        System.out.print("Introdueix nom del Departament: ");
        newDep.put("DNOMBRE", input());
        System.out.println();
        //localitat del departament
        System.out.print("Introdueix Localitzacio: ");
        newDep.put("LOC", input());

        return newDep;
    }
    //editar
    public static HashMap<String, String> editDep(int id) {
        HashMap<String, String> editDep = new HashMap<String, String>();
        //numero de departament
        editDep.put("DEPT_NO", String.valueOf(id));
        //nom del departament
        System.out.print("Introdueix nom del Departament: ");
        editDep.put("DNOMBRE", input());
        System.out.println();
        //localitat del departament
        System.out.print("Introdueix Localitzacio: ");
        editDep.put("LOC", input());

        return editDep;
    }
    //Show
    public static void showDept(int id) throws XMLDBException {
        ResourceSet result = servei.query("/departamentos/DEP_ROW[DEPT_NO=" + id + "]");
        ResourceIterator i;
        i = result.getIterator();
        if (!i.hasMoreResources()) {
            System.out.println("LA CONSULTA NO TORNA RES");
        }
        while (i.hasMoreResources()) {
            Resource r = i.nextResource();
            System.out.println((String) r.getContent());;
        }

    }
    //insert
    public static void insereixdep(HashMap<String, String> newDep) throws XMLDBException {
        String consulta = "update insert <DEP_ROW><DEPT_NO>" + newDep.get("DEPT_NO")
                + "</DEPT_NO><DNOMBRE>" + newDep.get("DNOMBRE")
                + "</DNOMBRE><LOC>" + newDep.get("LOC")
                + "</LOC></DEP_ROW> into /departamentos";
        ResourceSet result = servei.query(consulta);

    }
    //editar
    public static void modificadep(HashMap<String, String> editDep) throws XMLDBException {

        for (Map.Entry<String, String> entrySet : editDep.entrySet()) {
            String key = entrySet.getKey();
            String value = entrySet.getValue();
            if (!(key.equalsIgnoreCase("DEPT_NO"))) {
                String consulta = "update value /departamentos/DEP_ROW[DEPT_NO=" + editDep.get("DEPT_NO") + "]/" + key + " with '" + value + "'";
                ResourceSet result = servei.query(consulta);
            }

        }
    }
    //delete
    public static void esborradep(int id) throws XMLDBException {
        String consulta = "update delete /departamentos/DEP_ROW[DEPT_NO=" + id + "]";
        ResourceSet result = servei.query(consulta);
    }
}
