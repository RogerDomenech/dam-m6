-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para m6_a1
CREATE DATABASE IF NOT EXISTS `m6_a1` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `m6_a1`;

-- Volcando estructura para tabla m6_a1.alumnes
CREATE TABLE IF NOT EXISTS `alumnes` (
  `dni` varchar(10) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `dataneixament` varchar(45) DEFAULT NULL,
  `adrecapostal` varchar(45) DEFAULT NULL,
  `poblacions_cp` int(11) NOT NULL,
  PRIMARY KEY (`dni`,`poblacions_cp`),
  KEY `fk_alumnes_poblacions_idx` (`poblacions_cp`),
  CONSTRAINT `fk_alumnes_poblacions` FOREIGN KEY (`poblacions_cp`) REFERENCES `poblacions` (`cp`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla m6_a1.alumnes: ~2 rows (aproximadamente)
DELETE FROM `alumnes`;
/*!40000 ALTER TABLE `alumnes` DISABLE KEYS */;
INSERT INTO `alumnes` (`dni`, `nom`, `dataneixament`, `adrecapostal`, `poblacions_cp`) VALUES
	('3900000V', 'Test2', '1951/04/01', 'C/de la piruleta n4', 43205),
	('3900000Z', 'Test', '1950/04/01', 'C/de la piruleta n3', 43205);
/*!40000 ALTER TABLE `alumnes` ENABLE KEYS */;

-- Volcando estructura para tabla m6_a1.poblacions
CREATE TABLE IF NOT EXISTS `poblacions` (
  `cp` int(11) NOT NULL,
  `nompoblacio` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla m6_a1.poblacions: ~2 rows (aproximadamente)
DELETE FROM `poblacions`;
/*!40000 ALTER TABLE `poblacions` DISABLE KEYS */;
INSERT INTO `poblacions` (`cp`, `nompoblacio`) VALUES
	(43205, 'Reus'),
	(43206, 'Reus2');
/*!40000 ALTER TABLE `poblacions` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
