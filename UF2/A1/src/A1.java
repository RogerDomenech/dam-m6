import java.awt.List;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringJoiner;


public class A1 {
	static Connection con= null;
	static Driver driver = null;

	static String url = "jdbc:mysql://";
	static String host = "localhost:3306/";
	static String table = "m6_a1";
	static String options = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static String usuari="root";
	static String password="";
	static String taula1="alumnes";
	static String taula2="poblacions";
	static String[] fieldsAlumnes = {"dni","nom","dataneixament","adrecapostal","poblacions_cp"};


	public static void main(String[] args) {
		System.out.println("conectant");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat el controlador JDBC("+e.getMessage()+")");
			return;
		}
		try {
			con= DriverManager.getConnection(url+host+table+options,usuari,password);

			System.out.println("Connexi� realitzada usant Driver Manager");

			System.out.println("Dades de la base actual");
			System.out.println("Taula "+taula1);
			selectall(taula1);
			System.out.println("Taula "+taula2);
			selectall(taula2);
			boolean ctrl=true;
			//menu
			while(ctrl) {
				System.out.println("Indica Opcio:"+
						"\n 0-Sortir"+
						"\n 1-Introduir Alumne"+
						"\n 2-Modificar Alumne"+
						"\n 3-Esborrar Alumne"+
						"\n 4-Mostrar tots els Alumness"+
						"\n 5-Mostrar codis postals");
				Scanner teclat = new Scanner(System.in);
				try {
					int option = teclat.nextInt();
					if(option>=0&&option<=5) {
						switch (option) {
						case 0:
							System.out.println("bye,bye");
							ctrl=false;
							teclat.close();
							con.close();
							System.exit(1);
							break;
						case 1:
							//introduir alumne
							System.out.println("Introdueix DNI");
							String dni = teclat.next();
							System.out.println("Introdueix Nom");
							String nom =teclat.next();
							System.out.println("Introdueix data en fomart YYYY/MM/DD");
							String dataneixament = teclat.next();
							System.out.println("Introdueix Adre�a postal");
							teclat.nextLine();
							String adrecapostal = teclat.nextLine();
							System.out.println("Introduix codi postal");
							int poblacions_cp= teclat.nextInt();
							//Conjunt de dades	
							String[] valuesAlumnes = {dni,nom,dataneixament,adrecapostal,""+poblacions_cp};
							//insert
							insert(taula1,fieldsAlumnes,valuesAlumnes);
							break;
						case 2:
							//modificar alumne
							System.out.println("Introdueix DNI");
							dni = teclat.next();
							System.out.println("Introdueix Nom");
							nom =teclat.next();
							System.out.println("Introdueix data en fomart YYYY/MM/DD");
							dataneixament = teclat.next();
							System.out.println("Introdueix Adre�a postal");
							teclat.nextLine();
							adrecapostal = teclat.nextLine();
							System.out.println("Introduix codi postal");
							poblacions_cp= teclat.nextInt();
							//Conjunt de dades
							String[] valuesEditAlumnes = {dni,nom,dataneixament,adrecapostal,""+poblacions_cp};
							//edit
							edit(taula1,dni,fieldsAlumnes,valuesEditAlumnes);
							break;
						case 3:
							//esborrar alumne
							System.out.println("Introdueix DNI");
							dni = teclat.next();
							//delete
							delete("dni", taula1, dni);
							break;
						case 4:
							selectall(taula1);
							break;
						case 5:
							selectall(taula2);
							break;
						default:
							break;
						}
					}else {
						System.out.println("Opcio no valida");
					}
				}catch (Exception e) {
					System.out.println("Introdueix un numero");
				}
			}
		}catch (SQLException e) {
			System.out.println("Error "+e.getMessage());
		}

	}
	public static void edit(String table,String id, String[]fields, String[] values) {
		Statement stmt;
		String fieldsValue="";
		for (int i = 1; i < values.length; i++) {
			String value = "'"+values[i]+"'";
			fieldsValue+=fields[i]+"="+value+",";
		}
		fieldsValue = fieldsValue.substring(0,fieldsValue.length()-1);
		String query = "UPDATE "+table+" SET "+fieldsValue+" WHERE "+fields[0]+" like '"+id+"'";

		try {
			stmt = con.createStatement();
			stmt.execute(query);
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}

	}
	public static void insert(String table,String[]fields, String[] values) {
		Statement stmt;
		StringJoiner joinerFields = new StringJoiner(",");
		for (int i = 0; i < fields.length; i++) {
			joinerFields.add(fields[i]);
		}
		
		StringJoiner joinerValues = new StringJoiner(",");
		for (int i = 0; i < values.length; i++) {
			String value = "'"+values[i]+"'";
			joinerValues.add(value);
		}
		String query = "INSERT INTO "+table+" ("+joinerFields.toString()+") VALUES ("+joinerValues.toString()+");";

		try {
			stmt = con.createStatement();
			stmt.execute(query);
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}

	}
	public static void selectall(String table) {
		String query = "SELECT * FROM "+table;
		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();
			String headerColumms = "";
			for (int i = 1; i <= countColum; i++) {
				headerColumms+= metaData.getColumnName(i)+" | ";
			}
			System.out.println(headerColumms);
			headerColumms="";
			String valueColumms="";
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valueColumms+=result.getString(i)+" ";
					if(i==countColum) {
						valueColumms+="\n";
					}
				}
			}
			System.out.println(valueColumms);
			valueColumms="";
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}
	}
	public static ArrayList<String> select(String table,String[] fields) {

		StringJoiner joinerFields = new StringJoiner(",");
		for (int i = 0; i < fields.length; i++) {
			joinerFields.add(fields[i]);
		}
		String query = "SELECT "+joinerFields.toString()+" FROM "+table;
		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();

			ArrayList<String> valuesColummns= new ArrayList<String>();
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valuesColummns.add(result.getString(i));

				}
			}
			return valuesColummns;
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
			return null;
		}
	}
	public static ArrayList<String> find(String pk, String table, String id) {
		String query = "SELECT * FROM "+table+" WHERE "+pk+" like '"+id+"'";

		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();

			ArrayList<String> valuesColummns= new ArrayList<String>();
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valuesColummns.add(result.getString(i));

				}
			}
			return valuesColummns;
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
			return null;
		}

	}
	public static void  delete(String pk, String table, String id) {
		String query = "DELETE FROM "+table+" WHERE "+pk+" like '"+id+"'";
		Statement stmt;
		try {
			stmt = con.createStatement();
			stmt.executeUpdate(query);

		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());

		}

	}

}
