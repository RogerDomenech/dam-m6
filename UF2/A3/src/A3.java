import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class A3 {
	static Connection con;
	static String taula = "users";
	static boolean ctrl =true;
	public static void main(String[] args) {
		Connect connect = new Connect("m6_a3", "usuari", "usuari");
		con = connect.getCon();

		Scanner teclat = new Scanner (System.in);
		while (ctrl) {
			String user="";
			String pass ="";
			System.out.println("Contingut de la taula "+taula);
			selectall("users");
			System.out.println("\n");
			System.out.println("Seleciona mode:\n0-Sortir\n1-Consulta directe\n2-Consulta Preparada");
			int opcio = teclat.nextInt();
			if(opcio>=0 && opcio<=2) {
				switch (opcio) {
				case 0:
					//sortir
					ctrl=false;
					try {
						con.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					teclat.close();
					System.out.println("bye bye");
					System.exit(1);
					break;
				case 1:
					//Consulta directe
					boolean ctrlop1 = true;
					int countOp1=0;
					while (ctrlop1) {
						
						//Introduir dades
						teclat.nextLine();
						System.out.print("Introdueix nom usuari: ");
						user=teclat.nextLine();
						
						System.out.print("Introdueix password: ");
						pass=teclat.nextLine();
			
						//consulta directe
						String query= "SELECT id FROM "+taula+" WHERE name='"+user+"' AND password='"+pass+"';";
						
						Statement stmt;
						try {
							stmt = con.createStatement();
							ResultSet result = stmt.executeQuery(query);
							int id=0;
							while(result.next()) {
								id=result.getInt("id");
							}
							if(id!=0) {
								System.out.println("Correct");
								ctrlop1=false;
							}else {
								System.out.println("Incorrect");
								countOp1++;
								if(countOp1>=3) {
									ctrlop1=false;
								}
							}
						} catch (SQLException e1) {
							System.out.println(e1.getMessage());
							System.out.println("Incorrect");
							countOp1++;
						}
						System.out.println("\n");
					}
					break;
				case 2:
					//Consulta preparada
					boolean ctrlop2 = true;
					int countOp2=0;
					while (ctrlop2) {
						//Introduir dades
						teclat.nextLine();
						System.out.print("Introdueix nom usuari: ");
						user=teclat.nextLine();
						
						System.out.print("Introdueix password: ");
						pass=teclat.nextLine();
						
						//consulta preparada
						String pQuery = "SELECT id FROM "+taula+" WHERE name=? AND password=?;";
						//consulta preparada
						PreparedStatement pstmt;
						try {
							pstmt=con.prepareStatement(pQuery);
							pstmt.setString(1, user);
							pstmt.setString(2, pass);
							ResultSet preparedResult = pstmt.executeQuery();
							int id=0;
							while (preparedResult.next()) {
								id=preparedResult.getInt("id");
							}
							if(id!=0) {
								System.out.println("Correct");
								ctrlop2=false;
							}else {
								System.out.println("Incorrect");
								countOp2++;
								if(countOp2>=3) {
									ctrlop2=false;
								}
							}
						} catch (SQLException e1) {
							System.out.println("Incorrect");
							countOp2++;
						}
						System.out.println("\n");
					}
					break;

				default:
					break;
				}
			}

		} // end while

	}
	
	
	/**
	 * Funcio que mostra el contigut de la taula
	 * @param table
	 */
	public static void selectall(String table) {
		String query = "SELECT * FROM "+table;
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement(query);
			ResultSet result =stmt.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();
			String headerColumms = "";
			for (int i = 1; i <= countColum; i++) {
				headerColumms+= metaData.getColumnName(i)+" | ";
			}
			System.out.println(headerColumms);
			headerColumms="";
			String valueColumms="";
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valueColumms+=result.getString(i)+" ";
					if(i==countColum) {
						valueColumms+="\n";
					}
				}
			}
			System.out.println(valueColumms);
			valueColumms="";
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}
	}

}
