import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.StringJoiner;

public class Model {
	
	//propietat
	static Connection con;
	static String table;
	static ArrayList<String> fields;
	//constructor
	public Model(Connection con, String table) {
		this.con = con;
		this.table= table;
		this.fields=fields();
	}
	//metodes
	public static ArrayList<String>fields(){
		String query = "SELECT COLUMN_NAME "+
				"FROM INFORMATION_SCHEMA.COLUMNS"+
				"WHERE TABLE_NAME = '"+table+"'";
		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();
			ArrayList<String> valuesColummns= new ArrayList<String>();
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valuesColummns.add(result.getString(i));
				}
			}
			return valuesColummns;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;		
	}
	
	
	public static void insert(String table,String[]fields, String[] values) {
		Statement stmt;
		StringJoiner joinerFields = new StringJoiner(",");
		for (int i = 0; i < fields.length; i++) {
			joinerFields.add(fields[i]);
		}
		
		StringJoiner joinerValues = new StringJoiner(",");
		for (int i = 0; i < values.length; i++) {
			String value = "'"+values[i]+"'";
			joinerValues.add(value);
		}
		String query = "INSERT INTO "+table+" ("+joinerFields.toString()+") VALUES ("+joinerValues.toString()+");";

		try {
			stmt = con.createStatement();
			stmt.execute(query);
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}

	}
	public static ArrayList<String> select(String table,String[] fields) {

		StringJoiner joinerFields = new StringJoiner(",");
		for (int i = 0; i < fields.length; i++) {
			joinerFields.add(fields[i]);
		}
		String query = "SELECT "+joinerFields.toString()+" FROM "+table;
		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();

			ArrayList<String> valuesColummns= new ArrayList<String>();
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valuesColummns.add(result.getString(i));

				}
			}
			return valuesColummns;
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
			return null;
		}
	}
	public static void edit(String table,String id, String[]fields, String[] values) {
		Statement stmt;
		String fieldsValue="";
		for (int i = 1; i < values.length; i++) {
			String value = "'"+values[i]+"'";
			fieldsValue+=fields[i]+"="+value+",";
		}
		fieldsValue = fieldsValue.substring(0,fieldsValue.length()-1);
		String query = "UPDATE "+table+" SET "+fieldsValue+" WHERE "+fields[0]+" like '"+id+"'";

		try {
			stmt = con.createStatement();
			stmt.execute(query);
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}

	}
	
	public static void selectall(String table) {
		String query = "SELECT * FROM "+table;
		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();
			String headerColumms = "";
			for (int i = 1; i <= countColum; i++) {
				headerColumms+= metaData.getColumnName(i)+" | ";
			}
			System.out.println(headerColumms);
			headerColumms="";
			String valueColumms="";
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valueColumms+=result.getString(i)+" ";
					if(i==countColum) {
						valueColumms+="\n";
					}
				}
			}
			System.out.println(valueColumms);
			valueColumms="";
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}
	}
	
	public static ArrayList<String> find(String pk, String table, String id) {
		String query = "SELECT * FROM "+table+" WHERE "+pk+" like '"+id+"'";

		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();

			ArrayList<String> valuesColummns= new ArrayList<String>();
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valuesColummns.add(result.getString(i));

				}
			}
			return valuesColummns;
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
			return null;
		}

	}
	public static void  delete(String pk, String table, String id) {
		String query = "DELETE FROM "+table+" WHERE "+pk+" like '"+id+"'";
		Statement stmt;
		try {
			stmt = con.createStatement();
			stmt.executeUpdate(query);

		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());

		}

	}


}
