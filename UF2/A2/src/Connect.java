import java.sql.*;
import java.util.Properties;

public class Connect {
	//propietats
	static Connection con=null;
	static Driver driver=null;
	static Properties properties;
	static String url = "jdbc:mysql://";
	static String host = "localhost:3306/";
	static String db;
	static String options = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static String usuari;
	static String password;
	//constructor
	public Connect(String db,String user,String pass) {
		//montem la url
		this.db=db;
		this.url+=this.host+this.db+this.options;
		
		try {
			//controlador del DriverManeger
			driver= DriverManager.getDriver(url);
			//configuracio usuari
			properties = new Properties();
			properties.setProperty("user", user);
			properties.setProperty("password", pass);
			// creem instancia de connexio 
			con=driver.connect(url, properties);
			System.out.println("Connect to "+db);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error "+e.getMessage());
		}
	}
	//metodes

	public static void disconnect() {
		try {
			con.close();
			System.out.println("Disconect to "+db);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		    System.out.println("Error "+e.getMessage());
		}
	}


	public static Driver getDriver() {
		return driver;
	}

	public static void setDriver(Driver driver) {
		Connect.driver = driver;
	}

	public static Properties getProperties() {
		return properties;
	}

	public static void setProperties(Properties properties) {
		Connect.properties = properties;
	}

	public static String getUrl() {
		return url;
	}

	public static void setUrl(String url) {
		Connect.url = url;
	}

	public static String getDb() {
		return db;
	}

	public static void setDb(String db) {
		Connect.db = db;
	}
	

}
