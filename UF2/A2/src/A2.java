
import java.sql.*;



public class A2 {
	static Connection con= null;
	static Driver driver = null;

	static String url = "jdbc:mysql://";
	static String host = "localhost:3306/";
	static String table = "m6_a1";
	static String options = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static String usuari="usuari";
	static String password="usuari";
	static String taula1="alumnes";
	static String taula2="poblacions";
	static String[] fieldsAlumnes = {"dni","nom","dataneixament","adrecapostal","poblacions_cp"};


	@SuppressWarnings("null")
	public static void main(String[] args) {
		System.out.println("conectant");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat el controlador JDBC("+e.getMessage()+")");
			return;
		}
		try {
			//dades de mostra
			String[]dnis= {"3990000Q","3990001Q","3990002Q","3990003Q","3990004Q"};
			String[]noms= {"Pepe","Paco","Manuela","Josefa","Pepito"};
			String[]dates= {"1987/06/01","1987/11/21","1987/12/10","1987/5/21","1987/01/01"};
			String[]direccions= {"C/de la piruleta","C/de las chocolatinas"};
			int[] cps= {43205,43206};
			//conexio
			con= DriverManager.getConnection(url+host+table+options,usuari,password);
			//Avis d'usuari
			System.out.println("Connexi� realitzada usant Driver Manager");
			System.out.println("Dades de la base actual: "+table);
			System.out.println("Taula: "+taula1);
			//Mostra el contigut de la taula
			selectall(taula1);
			//Preparar les consultes
			Statement stmt=null;
			try {
				//deshabilitem AutoCommit
				con.setAutoCommit(false);
				stmt = con.createStatement();
				//Consultes de prova
				String sql= "INSERT INTO alumnes (dni,nom,dataneixament,adrecapostal,poblacions_cp) VALUES(";
				sql+="'"+dnis[0]+"','"+noms[0]+"','"+dates[0]+"','"+direccions[0]+"','"+cps[0]+"');";
				String sql2 = "DELETE FROM "+taula1+" WHERE dni like '"+dnis[0]+"';";
				String sql3 = "UPDATE "+taula1+" SET nom='Roquefeler' WHERE dni='"+dnis[0]+"';";
				String sql4 ="SELECT * FROM "+taula1+";";
				//executa borrar
				stmt.executeUpdate(sql2);
				//retard de 10s
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//executa insert
				stmt.executeUpdate(sql);
				//executa un select all
				ResultSet rs =stmt.executeQuery(sql4);
				//resultats del select All
				while (rs.next()) {
					System.out.println("Nom del Alumne introduit: "+rs.getString("nom"));

				}
				//Executa un Update
				stmt.executeUpdate(sql3);
				//realitza el commit
				con.commit();
				//habilita el autoCommit
				con.setAutoCommit(true);
			}catch (SQLException e) {
				//realitza el rollback
				con.rollback();
				//avis del usuari
				System.out.println("Realitzat Rollback \nERROR: " +e.getMessage());
			}finally {
				//si el stmt esta null i la conexio esta oberta
				//tancara la conexio 
				if(stmt==null&& !stmt.isClosed()) {
					System.out.println("Desconectant...");
					con.close();
				}

			}
			//mostra tota la taula
			selectall(taula1);
			//tanca conexio
			con.close();
			//avis de tancament
			System.out.println("Conexio tancada");
		}catch (SQLException e) {
			System.out.println("Error "+e.getMessage());
		}



	}
	
	/**
	 * Mostra tots els elements d'una taula per consola
	 * @param table nom de la taula a mostrar
	 */
	public static void selectall(String table) {
		String query = "SELECT * FROM "+table;
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement(query);
			ResultSet result =stmt.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			//obtenim el numero de columenes del metadata
			int countColum =metaData.getColumnCount();
			//montem la capcelera de la taula
			String headerColumms = "";
			for (int i = 1; i <= countColum; i++) {
				headerColumms+= metaData.getColumnName(i)+" | ";
			}
			//mosterm capcelerara
			System.out.println(headerColumms);
			//buidem variable
			headerColumms="";
			//monetem Valors de les columnes
			String valueColumms="";
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					//afegim espai a cada element
					valueColumms+=result.getString(i)+" ";
					//si arribem al final afegim un salt de linea
					if(i==countColum) {
						valueColumms+="\n";
					}
				}
			}
			//mostrem els valors de les columenes
			System.out.println(valueColumms);
			//borrem els valors de les columens
			valueColumms="";
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}
	}

}
