import java.awt.List;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringJoiner;


public class A2_bak {
	static Connection con= null;
	static Driver driver = null;

	static String url = "jdbc:mysql://";
	static String host = "localhost:3306/";
	static String table = "m6_a1";
	static String options = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static String usuari="usuari";
	static String password="usuari";
	static String taula1="alumnes";
	static String taula2="poblacions";
	static String[] fieldsAlumnes = {"dni","nom","dataneixament","adrecapostal","poblacions_cp"};


	public static void main(String[] args) {
		System.out.println("conectant");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch (ClassNotFoundException e) {
			System.out.println("No s'ha trobat el controlador JDBC("+e.getMessage()+")");
			return;
		}
		try {
			String[]dnis= {"3990000Q","3990001Q","3990002Q","3990003Q","3990004Q"};
			String[]noms= {"Pepe","Paco","Manuela","Josefa","Pepito"};
			String[]dates= {"1987/06/01","1987/11/21","1987/12/10","1987/5/21","1987/01/01"};
			String[]direccions= {"C/de la piruleta","C/de las chocolatinas"};
			int[] cps= {43205,43206};

			con= DriverManager.getConnection(url+host+table+options,usuari,password);

			System.out.println("Connexi� realitzada usant Driver Manager");

			System.out.println("Dades de la base actual");
			System.out.println("Taula "+taula1);
			selectall(taula1);
			
				//introduir alumne
				PreparedStatement insertAlummnes = insert(taula1);
				for (int i = 0; i < dnis.length; i++) {
					int rand = (int)(Math.random()*((1-0)+1));
					insertAlummnes.setString(1, dnis[i]);
					insertAlummnes.setString(2, noms[i]);
					insertAlummnes.setString(3, dates[i]);
					insertAlummnes.setString(4, direccions[rand]);
					insertAlummnes.setInt(5, cps[rand]);
					//interrumpim la execucio
					System.out.println("Alumne:"+
							"\nNom: "+noms[i]+"\nDni: "+dnis[i]+
							"\nData Neixament: "+dates[i]+"\nDireccio: "+direccions[rand]+
							"\nCP: "+cps[rand]);
					
						get(insertAlummnes);
					
				}
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//esborrar alumne

				String[] id = {"dni"};
				ArrayList<String>selecIds = select(taula1, id);
				for (String selectID : selecIds) {
					PreparedStatement del=delete("dni", taula1, selectID);
					get(del);
				}
				
				//mostra la taula d'alumnes
				selectall(taula1);
			

		}catch (SQLException e) {
			System.out.println("Error "+e.getMessage());
		}



	}
	public static ArrayList<String>fields(String table){
		String query = "SELECT COLUMN_NAME "+
				"FROM INFORMATION_SCHEMA.COLUMNS "+
				"WHERE TABLE_NAME = '"+table+"'";
		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();
			ArrayList<String> valuesColummns= new ArrayList<String>();
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valuesColummns.add(result.getString(i));
				}
			}
			return valuesColummns;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;		
	}
	public static void selectall(String table) {
		String query = "SELECT * FROM "+table;
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement(query);
			ResultSet result =stmt.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();
			String headerColumms = "";
			for (int i = 1; i <= countColum; i++) {
				headerColumms+= metaData.getColumnName(i)+" | ";
			}
			System.out.println(headerColumms);
			headerColumms="";
			String valueColumms="";
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valueColumms+=result.getString(i)+" ";
					if(i==countColum) {
						valueColumms+="\n";
					}
				}
			}
			System.out.println(valueColumms);
			valueColumms="";
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}
	}
	public static PreparedStatement insert(String table) {
		ArrayList<String>fieldsTable = fields(table);
		StringJoiner joinerFields = new StringJoiner(",");
		StringJoiner joinerValues = new StringJoiner(",");
		for (int i = 0; i < fieldsTable.size(); i++) {
			joinerFields.add(fieldsTable.get(i));
			String value = "?";
			joinerValues.add(value);
		}

		String query = "INSERT INTO "+table+" ("+joinerFields.toString()+") VALUES ("+joinerValues.toString()+");";
		

		try {

			PreparedStatement stm = con.prepareStatement(query);
			return stm;
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}
		return null;
	}
	public static void get(PreparedStatement stm) {
		try {
			stm.executeUpdate();
		} catch (SQLException e) {
			
			System.out.println("Error "+e.getMessage());
		}
	}



	public static ArrayList<String> select(String table,String[] fields) {

		StringJoiner joinerFields = new StringJoiner(",");
		for (int i = 0; i < fields.length; i++) {
			joinerFields.add(fields[i]);
		}
		String query = "SELECT "+joinerFields.toString()+" FROM "+table;
		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();

			ArrayList<String> valuesColummns= new ArrayList<String>();
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valuesColummns.add(result.getString(i));

				}
			}
			return valuesColummns;
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
			return null;
		}
	}

	public static PreparedStatement  delete(String pk, String table, String id) {
		String query = "DELETE FROM "+table+" WHERE "+pk+" like '"+id+"'";
		PreparedStatement stmt;
		try {
		
			stmt = con.prepareStatement(query);
			return stmt;

		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());

		}
		return null;
	}

	/******/
	public static ArrayList<String> find(String pk, String table, String id) {
		String query = "SELECT * FROM "+table+" WHERE "+pk+" like '"+id+"'";

		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet result =stmt.executeQuery(query);
			ResultSetMetaData metaData = result.getMetaData();
			int countColum =metaData.getColumnCount();

			ArrayList<String> valuesColummns= new ArrayList<String>();
			while(result.next()) {
				for (int i = 1; i <= countColum; i++) {
					valuesColummns.add(result.getString(i));

				}
			}
			return valuesColummns;
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
			return null;
		}

	}
	public static void edit(String table,String id, String[]fields, String[] values) {
		Statement stmt;
		String fieldsValue="";
		for (int i = 1; i < values.length; i++) {
			String value = "'"+values[i]+"'";
			fieldsValue+=fields[i]+"="+value+",";
		}
		fieldsValue = fieldsValue.substring(0,fieldsValue.length()-1);
		String query = "UPDATE "+table+" SET "+fieldsValue+" WHERE "+fields[0]+" like '"+id+"'";

		try {
			stmt = con.createStatement();
			stmt.execute(query);
		} catch (SQLException e) {
			System.out.println("Error STMT "+e.getMessage());
		}

	}

}
