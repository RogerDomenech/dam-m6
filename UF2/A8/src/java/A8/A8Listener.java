/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A8;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Web application lifecycle listener.
 *
 * @author Kitsune
 */
public class A8Listener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        com.objectdb.Enhancer.enhance("A8.*");
        EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("$objectdb/db/A8.odb");
        sce.getServletContext().setAttribute("emf", emf);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
          EntityManagerFactory emf =
            (EntityManagerFactory)sce.getServletContext().getAttribute("emf");
        emf.close();
    }
}
