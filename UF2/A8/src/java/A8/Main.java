/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A8;

import javax.persistence.*;
import java.util.*;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Open a database connection
        // (create a new database if it doesn't exist yet):
        EntityManagerFactory emf
                = Persistence.createEntityManagerFactory("$objectdb/db/p2.odb");
        EntityManager em = emf.createEntityManager();

        // Store 1000 Point objects in the database:
        //DADES TEST Client
        String[]DNIs = {"000000000Z","000000000Y","000000000X"};
        String[]noms = {"Paco","Pepe","Perico"};
        String[]tels = {"600000000","600000001","600000002"};
        
        //DADES TEST Vehicle
        String[]matricules = {"0000AAA","0000BBB","0000CCC"};
        String[]marques = {"Renault","Ford","Citroen"};
        String[]models = {"Clio","Fiesta","Picasso"};
        Integer[]numPortes = {3,3,5};
        String[]carburants = {"Gasolina","Diessel","Diessel"};
        Integer[]kms = {10000,15000,5000};
        Date[]lastVist= {new Date(),new Date(),new Date()};
        Boolean[]isEmpresa = {true,false,false};
        
         em.getTransaction().begin();
        //for (int i = 0; i < 3; i++) {
         //Client c = new Client(DNIs[i], noms[i], tels[i]);
         //em.persist(c);
         //  Vehicle v = new Vehicle(matricules[i], models[i], marques[i], numPortes[i], carburants[i], kms[i],lastVist[i],isEmpresa[i]);
          // em.persist(v);
        // }
         //em.getTransaction().commit();
        // Find the number of Point objects in the database:
        Query q1 = em.createQuery("SELECT COUNT(c) FROM Client c");
         System.out.println("Total Clients: " + q1.getSingleResult());
        // Find the average X value:
        /*Query q2 = em.createQuery("SELECT AVG(p.x) FROM Point p");
         System.out.println("Average X: " + q2.getSingleResult());*/
        // Retrieve all the Point objects from the database:
         TypedQuery<Vehicle> vquery = 
         em.createQuery("SELECT v FROM Vehicle v",Vehicle.class);
         List<Vehicle> vresult = vquery.getResultList();
         TypedQuery<Client> query =
         em.createQuery("SELECT c FROM Client c", Client.class);
         List<Client> results = query.getResultList();
         int count = 0;
         for (Client p : results) {
         //p.setVehicle(vresult.get(count));
         //   count++;
         //em.persist(p);
         System.out.println(p);
         System.out.println(p.getVehicle());
         }
        //em.getTransaction().commit();
        // Close the database connection:
         em.close();
         emf.close();
    }
}
