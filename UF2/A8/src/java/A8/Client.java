/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A8;

/**
 *
 * @author Kitsune
 */
import java.io.Serializable;
import javax.persistence.*;

@Entity
public class Client implements Serializable{
    
    @Id 
    private String DNI;
    
    private String nom;
    private String tel;
    
    @ManyToOne
    private Vehicle vehicle;
    //constructor

    public Client() {
    }

    public Client(String DNI, String nom, String tel) {
        this.DNI = DNI;
        this.nom = nom;
        this.tel = tel;
    }
    
    //get set

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
    
    
    
   /* @Override
    public String toString(){
        return ("\nNom Client: "+getNom()+
                "\nDNI/NIF: "+getDNI()+
                "\nTelefon: "+getTel());
    }*/
    
}
