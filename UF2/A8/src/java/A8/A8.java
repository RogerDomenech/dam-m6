/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A8;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kitsune
 */
@WebServlet(name = "A8", urlPatterns = {"/A8"})
public class A8 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Obtain a database connection:
        EntityManagerFactory emf
                = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();
        try {
            Map<String, String[]> requestMap = request.getParameterMap();
            //test del contingut del request
            System.out.print(requestMap);
            //obre conexio
            em.getTransaction().begin();

            //CLIENTS
            // add client
            if (requestMap.containsKey("client[DNI]")) {
                try {
                    String DNI = requestMap.get("client[DNI]")[0];
                    String nom = requestMap.get("client[name]")[0];
                    String tel = requestMap.get("client[tel]")[0];
                    // System.out.println(DNI+" "+nom+" "+tel);
                    em.persist(new Client(DNI, nom, tel));
                    em.getTransaction().commit();
                } catch (Exception e) {
                    System.err.println("ERROR: " + e.getMessage());
                }
            }

            //Mostra Clients
            if (requestMap.containsKey("clients") || requestMap.containsKey("client[DNI]")) {
                // Display the list of guests:
                List<Client> clientList = em.createQuery(
                        "SELECT c FROM Client c", Client.class).getResultList();
                request.setAttribute("clients", clientList);
                request.getRequestDispatcher("/view/clients/client.jsp")
                        .forward(request, response);
            }
            //Edit clients
            // envia formulari de editar
            if (requestMap.containsKey("cedit")) {
                String dni = requestMap.get("cedit")[0];
                System.out.print(dni);
                try {
                    List<Client> client = em.createQuery(
                            "SELECT c FROM Client c where c.DNI = '" + dni + "'", Client.class).getResultList();
                    request.setAttribute("edit_client", client);
                    request.getRequestDispatcher("/view/clients/client_edit.jsp")
                            .forward(request, response);
                } catch (Exception e) {
                    System.err.println("ERROR: " + e.getMessage());
                }

            }
            //guardar els canvi del editar
            if (requestMap.containsKey("clientEdit[DNI]")) {
                System.out.print("Edit");
                String dniEdit = requestMap.get("clientEdit[DNI]")[0];
                String nomEdit = requestMap.get("clientEdit[name]")[0];
                String telEdit = requestMap.get("clientEdit[tel]")[0];

                try {
                    List<Client> client = em.createQuery(
                            "SELECT c FROM Client c where c.DNI = '" + dniEdit + "'", Client.class).getResultList();
                    //edita el objecte
                    Client editatClient = client.get(0);
                    //comprova que els camps estan plens avans de editar
                    if (nomEdit != null && nomEdit.length() > 1) {
                        editatClient.setNom(nomEdit);
                    }
                    if (telEdit != null && telEdit.length() > 1) {
                        editatClient.setTel(telEdit);
                    }
                    //guarda els canvis
                    em.persist(editatClient);
                    em.getTransaction().commit();
                    //redirect a client
                    response.sendRedirect(request.getContextPath() + "/A8?clients");
                } catch (Exception e) {
                    System.err.println("ERROR: " + e.getMessage());
                }

            }
            //Del clients
            if (requestMap.containsKey("cdel")) {
                String idDel = requestMap.get("cdel")[0];
                System.out.print(idDel);

                try {
                    List<Client> client = em.createQuery(
                            "SELECT c FROM Client c where c.DNI = '" + idDel + "'", Client.class).getResultList();
                    //edita el objecte
                    Client delClient = client.get(0);
                    em.remove(delClient);
                    em.getTransaction().commit();
                    //redirect a client
                    response.sendRedirect(request.getContextPath() + "/A8?clients");
                } catch (Exception e) {
                    System.err.println("ERROR: " + e.getMessage());
                }
            }
            //VEHICLES
            //add vehicles
            if (requestMap.containsKey("vehicle[matricula]")) {
                System.out.print("add");
                try {
                    String matricula = requestMap.get("vehicle[matricula]")[0];
                    String model = requestMap.get("vehicle[model]")[0];
                    String marca = requestMap.get("vehicle[marca]")[0];
                    Integer numPortes = Integer.parseInt(requestMap.get("vehicle[portes]")[0]);
                    String carburant = requestMap.get("vehicle[carburant]")[0];
                    Integer km = Integer.parseInt(requestMap.get("vehicle[kms]")[0]);

                    SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                    Date ultVisita = formatDate.parse(requestMap.get("vehicle[ultVist]")[0]);
                    Boolean vehileEmpresa = (requestMap.get("vehicle[esEmpresa]")[0].equalsIgnoreCase("True")) ? true : false;
                    Vehicle newVehicle = new Vehicle(matricula, model, marca, numPortes, carburant, km, ultVisita, vehileEmpresa);
                    em.persist(newVehicle);

                    //assignem el vehicle al client si existeix
                    if (requestMap.get("vehicle[client]")[0] != null && requestMap.get("vehicle[client]")[0].length() > 1) {
                        //busquem el client per el id 
                        List<Client> client = em.createQuery(
                                "SELECT c FROM Client c where c.DNI = '" + requestMap.get("vehicle[client]")[0] + "'", Client.class).getResultList();
                        //edita el objecte
                        Client vehicleClient = client.get(0);
                        vehicleClient.setVehicle(newVehicle);
                        em.persist(vehicleClient);
                    }
                    em.getTransaction().commit();
                } catch (Exception e) {
                    System.err.println("ERROR Vehicle ADD: " + e.getMessage());
                }
            }

            //edit vehicles
            // envia formulari de editar
            if (requestMap.containsKey("vedit")) {
                String matricula = requestMap.get("vedit")[0];
                System.out.print(matricula);
                try {
                    List<Vehicle> vehicle = em.createQuery(
                            "SELECT v FROM Vehicle v where v.matricula = '" + matricula + "'", Vehicle.class).getResultList();
                    request.setAttribute("edit_vehicle", vehicle);
                    // afegim els clients
                    // llistat de client que tenen aquesta matricula
                    Client selectClient = new Client();

                    List<Client> clients = em.createQuery(
                            "SELECT c FROM Client c", Client.class).getResultList();

                    for (Client client : clients) {
                        Vehicle findVehicle = client.getVehicle();

                        if (findVehicle != null && findVehicle.getMatricula().equalsIgnoreCase(matricula)) {
                            selectClient = client;
                        }

                    }

                    request.setAttribute("client", selectClient);
                    // llistat de clients sense vehicle
                    List<Client> clientlist = em.createQuery(
                            "SELECT c FROM Client c where c.vehicle == null ", Client.class).getResultList();
                    request.setAttribute("clients", clientlist);
                    request.getRequestDispatcher("/view/vehicles/vehicle_edit.jsp")
                            .forward(request, response);
                } catch (Exception e) {
                    System.err.println("ERROR: " + e.getMessage());
                }
            }
            //guardar els canvi del editar
            if (requestMap.containsKey("vehicleEdit[matricula]")) {
                System.out.print("Edit");
                String matricula = requestMap.get("vehicleEdit[matricula]")[0];
                String model = requestMap.get("vehicleEdit[model]")[0];
                String marca = requestMap.get("vehicleEdit[marca]")[0];
                Integer numPortes = null;
                String DNIclient = requestMap.get("vehicleEdit[client]")[0];
                System.out.print(DNIclient);
                if (requestMap.get("vehicleEdit[portes]")[0] != null && requestMap.get("vehicleEdit[portes]")[0].length() > 1) {
                    numPortes = Integer.parseInt(requestMap.get("vehicleEdit[portes]")[0]);
                }
                String carburant = requestMap.get("vehicleEdit[carburant]")[0];
                Integer km = null;
                if (requestMap.get("vehicleEdit[kms]")[0] != null && requestMap.get("vehicleEdit[kms]")[0].length() > 1) {
                    km = Integer.parseInt(requestMap.get("vehicleEdit[kms]")[0]);
                }

                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");

                Date ultVisita = null;
                try {
                    if (requestMap.get("vehicleEdit[ultVist]")[0] != null && requestMap.get("vehicleEdit[ultVist]")[0].length() > 1) {
                        ultVisita = formatDate.parse(requestMap.get("vehicleEdit[ultVist]")[0]);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(A8.class.getName()).log(Level.SEVERE, null, ex);
                }
                Boolean vehileEmpresa = null;
                if (requestMap.get("vehicleEdit[esEmpresa]")[0] != null && requestMap.get("vehicleEdit[esEmpresa]")[0].length() > 1) {
                    vehileEmpresa = (requestMap.get("vehicleEdit[esEmpresa]")[0].equalsIgnoreCase("True")) ? true : false;
                }

                try {
                    List<Vehicle> vehicle = em.createQuery(
                            "SELECT v FROM Vehicle v where v.matricula  = '" + matricula + "'", Vehicle.class).getResultList();
                    //edita el objecte
                    Vehicle editatVehicle = vehicle.get(0);
                    //comprova que els camps estan plens avans de editar
                    if (model != null && model.length() > 1) {
                        editatVehicle.setModel(model);
                    }
                    if (marca != null && marca.length() > 1) {
                        editatVehicle.setMarca(marca);
                    }
                    if (numPortes != null) {
                        editatVehicle.setNumPortes(numPortes);
                    }
                    if (carburant != null && carburant.length() > 1) {
                        editatVehicle.setCarburant(carburant);
                    }
                    if (km != null) {
                        editatVehicle.setKm(km);
                    }
                    if (ultVisita != null) {
                        editatVehicle.setUltVisita(ultVisita);
                    }
                    if (vehileEmpresa != null) {
                        editatVehicle.setVehileEmpresa(vehileEmpresa);
                    }
                    //guarda els canvis
                    em.persist(editatVehicle);
                    em.getTransaction().commit();
                    //editem el client si existeix
                    if (DNIclient != null && DNIclient.length() > 1 && matricula != null && matricula.length() > 1) {

                        em.getTransaction().begin();
               
                        //busca el client 
                        Client clientRequest = em.find(Client.class, DNIclient);
                        //busca el vehicle actual

                        Vehicle thisVehicle = em.find(Vehicle.class, matricula);

                        // mira si el vehicle te propietari
                        List<Client> client = em.createQuery(
                                "SELECT c FROM Client c where c.vehicle.matricula = '" + thisVehicle.getMatricula() + "'", Client.class).getResultList();
                        //edita el objecte
                        System.out.print((client != null) ? "exist" : "no exist");
                        System.out.print(client.size());
                        if (client.size() > 1) {
                            Client clientActual = client.get(0);
                      
                            System.out.print((client != null) ? ""+clientActual.getNom() : "null");
                        // en cas afirmatiu borra la relacio amb el seu propietari 
                            // i assigna al nou propietari 
                            if (clientActual != null) {
                                //em.getTransaction().begin();
                                clientActual.setVehicle(null);
                                em.persist(clientActual);
                                //em.getTransaction().commit();
                            }
                        }
             
                        clientRequest.setVehicle(editatVehicle);
                        //guarda els canvis
                        em.persist(clientRequest);
                        em.getTransaction().commit();
                    }

                    //redirect a client
                    response.sendRedirect(request.getContextPath() + "/A8?vehicles");
                } catch (Exception e) {
                    System.err.println("ERROR: " + e.getMessage());
                }

            }
            //del vehicles
            if (requestMap.containsKey("vdel")) {
                String idDel = requestMap.get("vdel")[0];
                System.out.print(idDel);

                try {
                    List<Vehicle> vehicle = em.createQuery(
                            "SELECT v FROM Vehicle v where v.matricula = '" + idDel + "'", Vehicle.class).getResultList();
                    //edita el objecte
                    Vehicle delVehicle = vehicle.get(0);
                    em.remove(delVehicle);
                    em.getTransaction().commit();
                    //redirect a client
                    response.sendRedirect(request.getContextPath() + "/A8?vehicles");
                } catch (Exception e) {
                    System.err.println("ERROR: " + e.getMessage());
                }
            }
            //mostra vehicles
            if (requestMap.containsKey("vehicles") || requestMap.containsKey("vehicle[matricula]")) {
                // llistat de vehicles
                List<Vehicle> vehiclesList = em.createQuery(
                        "SELECT v FROM Vehicle v", Vehicle.class).getResultList();
                request.setAttribute("vehicles", vehiclesList);
                // llistat de clients sense vehicle
                List<Client> clientlist = em.createQuery(
                        "SELECT c FROM Client c where c.vehicle == null ", Client.class).getResultList();
                request.setAttribute("clients", clientlist);
                //redirect 
                request.getRequestDispatcher("/view/vehicles/vehicle.jsp")
                        .forward(request, response);
            }
            //MAIN
            if(requestMap.containsKey("main")){
                List<Client> clientList = em.createQuery(
                        "SELECT c FROM Client c", Client.class).getResultList();
                request.setAttribute("clients", clientList);
                request.getRequestDispatcher("/view/main.jsp")
                        .forward(request, response);
            }

        } finally {
            // Close the database connection:
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
