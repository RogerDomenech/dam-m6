/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A8;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Vehicle implements Serializable{
    @Id
    private String matricula;
    
    private String model;
    private String marca;
    private Integer numPortes;
    private String carburant;
    private Integer km;
    private Date ultVisita;
    private Boolean vehileEmpresa;
  
    
    //constructor

    public Vehicle() {
    }

    public Vehicle(String matricula, String model, String marca, Integer numPortes, String carburant, Integer km, Date ultVisita, Boolean vehileEmpresa) {
        this.matricula = matricula;
        this.model = model;
        this.marca = marca;
        this.numPortes = numPortes;
        this.carburant = carburant;
        this.km = km;
        this.ultVisita = ultVisita;
        this.vehileEmpresa = vehileEmpresa;
    
    }

   
    // get set 

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getNumPortes() {
        return numPortes;
    }

    public void setNumPortes(Integer numPortes) {
        this.numPortes = numPortes;
    }

    public String getCarburant() {
        return carburant;
    }

    public void setCarburant(String carburant) {
        this.carburant = carburant;
    }

    public Integer getKm() {
        return km;
    }

    public void setKm(Integer km) {
        this.km = km;
    }

    public Date getUltVisita() {
        return ultVisita;
    }

    public void setUltVisita(Date ultVisita) {
        this.ultVisita = ultVisita;
    }

    public Boolean getVehileEmpresa() {
        return vehileEmpresa;
    }

    public void setVehileEmpresa(Boolean vehileEmpresa) {
        this.vehileEmpresa = vehileEmpresa;
    }

    @Override
    public String toString(){
        String esEmpresa =(getVehileEmpresa())?"SI":"NO";
        return ("\nMatricula: "+getMatricula()+
                "\nMarca: "+getMarca()+
                "\nModel: "+getModel()+
                "\nCarburarnt: "+getCarburant()+
                "\nKm: "+getKm()+
                "\nUltima visita: "+getUltVisita()+
                "\nVehicle d'Empresa: "+esEmpresa);
    }
    
}
