
<%@page import="A8.Client"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*,A8.Vehicle"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="included/css/bootstrap.css">
        <script src="included/js/bootstrap.js"></script>
    </head>
    <body>
        <nav class="nav">
            <a class="nav-link" href="./">Index</a>
            <a class="nav-link" active href="A8?main">Principal</a>
            <a class="nav-link" href="A8?clients">Clients</a>
            <a class="nav-link" href="A8?vehicles">Vehicles</a>
           
        </nav>
        <div class="content ml-2 mr-2 col">

            <div class="row">
                <h2>Clients</h2>
            </div>

            <div class="row">
                <%
                    @SuppressWarnings(  "unchecked")
                    List<Client> clients = (List<Client>) request.getAttribute("clients");

                    if (clients == null) {
                %><h3> No hi ha Clients</h3>
                <%} else {%>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nom</th>
                            <th scope="col">DNI</th>
                            <th scope="col">Telefon</th>
                            <th scope="col">Vehicle</th>

                        </tr>
                    </thead>
                    <tbody>
                        <%for (Client client : clients) {
                                String nom = client.getNom();
                                String DNI = client.getDNI();
                                String tel = client.getTel();
                                Vehicle vehicle = client.getVehicle();
                                if (vehicle != null) {
                                    System.out.print(vehicle.getMatricula());
                                }
                        %>

                        <tr>
                            <td scope="row"> <%= nom%> </td>
                            <td> <%= DNI%> </td>
                            <td> <%= tel%> </td>
                            <td> 
                                <% if (vehicle != null) {
                                        String matricula = vehicle.getMatricula();
                                        String model = vehicle.getModel();
                                        String marca = vehicle.getMarca();
                                        Integer numPortes = vehicle.getNumPortes();
                                        String carburant = vehicle.getCarburant();
                                        Integer km = vehicle.getKm();
                                        Date ultVisita = vehicle.getUltVisita();
                                        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                                        String printdate = formatDate.format(ultVisita);
                                        Boolean esEmpresa = vehicle.getVehileEmpresa();
                                        %>
                                         <ul>
                                            <li>Matricula: <%= matricula%></li>
                                            <li>Model: <%= model %></li>
                                            <li>Marca: <%= marca%></li>
                                            <li>Portes: <%= numPortes%></li>
                                            <li>Carburant: <%= carburant%></li>
                                            <li>KM: <%= km %></li>
                                            <li>Ultima visita: <%= printdate%></li>
                                            <li>Vehicle d'empresa: <%= (esEmpresa) ? "Si" : "No"%></li>
                                         </ul>                              
                                    <% }else{ %>
                                    <b>No hi ha vehicle</b>
                                    <% }%>
                            </td>
                        </tr> 
                        <%
                                }
                            }%>
                    </tbody>
                </table>
            </div>
        </div>

    </body>
</html>
