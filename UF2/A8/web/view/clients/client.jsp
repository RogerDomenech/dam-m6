<%-- 
    Document   : newjsp
    Created on : 11-mar-2020, 20:07:17
    Author     : Kitsune
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*,A8.Client"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="included/css/bootstrap.css">
        <script src="included/js/bootstrap.js"></script>
    </head>
    <body>
        <nav class="nav">
            <a class="nav-link" href="./">Index</a>
            <a class="nav-link" href="A8?main">Principal</a>
            <a class="nav-link" active href="A8?clients">Clients</a>
            <a class="nav-link" href="A8?vehicles">Vehicles</a>
        </nav>
        <div class="content ml-2 mr-2 col">

            <div class ="row">
                <h1>Clients</h1>
            </div>
            <div class="row">
                <!-- Default form register -->
                <form class="border border-light" action="A8">

                    <p class="h4 mb-4">Nou Client</p>

                    <div class="form-row mb-4">
                        <div class="col">
                            <!-- First name -->
                            <input type="text" class="form-control" placeholder="Nom del client" name="client[name]" required>
                        </div>
                        <div class="col">
                            <!-- Last name -->
                            <input type="text" class="form-control" placeholder="DNI del client" name="client[DNI]" required>
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="telefon del client" name="client[tel]" required>
                        </div>
                        <div class="col">
                            <!-- Sign up button -->
                            <button class="btn btn-info btn-block" type="submit">Add</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Default form register -->

            <hr>
            <div class="row">
                <h2>Clients</h2>
            </div>

            <div class="row">
                <%
                    @SuppressWarnings(  "unchecked")
                    List<Client> clients = (List<Client>) request.getAttribute("clients");

                    if (clients == null) {
                %><h3> No hi ha Clients</h3>
                <%} else {%>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nom</th>
                            <th scope="col">DNI</th>
                            <th scope="col">Telefon</th>
                            <th scope="col">Accions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%for (Client client : clients) {
                                String nom = client.getNom();
                                String DNI = client.getDNI();
                                String tel = client.getTel();

                        %>

                        <tr>
                            <td scope="row"> <%= nom%> </td>
                            <td> <%= DNI%> </td>
                            <td> <%= tel%> </td>
                            <td> <a class="mx-1" href="./A8?cedit=<%=DNI%>">Editar</a><a class="mx-1" href="./A8?cdel=<%=DNI%>">Borrar</a> </td>
                        </tr> 
                        <%
                                }
                            }%>
                    </tbody>
                </table>
            </div>
        </div>

    </body>
</html>
