<%-- 
    Document   : newjsp
    Created on : 11-mar-2020, 20:07:17
    Author     : Kitsune
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*,A8.Client"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="included/css/bootstrap.css">
        <script src="included/js/bootstrap.js"></script>
    </head>
    <body>
        <div class="content ml-2 mr-2 col">
            <% @SuppressWarnings(  "unchecked")
                    List<Client> client_edit = (List<Client>) request.getAttribute("edit_client");
                    String dni = client_edit.get(0).getDNI();
                    String nom = client_edit.get(0).getNom();
                    String tel = client_edit.get(0).getTel();
            %>
            <div class ="row">
                <h1>Edita Clients</h1>
            </div>
            <div class="row">
                <!-- Default form register -->
                <form class="border border-light" action="A8">

                    <p class="h4 mb-4">Edita Client: <%= nom %></p>

                    <div class="form-inline mb-4">
                        <div class="form-group mx-2">
                            <label>Nom del Client: </label>
                            <input type="text" class="form-control" placeholder="<%= nom %>" name="clientEdit[name]"  >
                        </div>
                       <!-- <div class="form-group mx-2">-->
                            <!--<label>DNI del Client: </label>-->
                            <input type="hidden" class="form-control" placeholder="<%= dni %>" name="clientEdit[DNI]" value="<%= dni %>" required>
                       <!--</div>-->
                        <div class="form-group mx-2">
                            <label>Telefon del Client:</label>
                            <input type="text" class="form-control" placeholder="<%= tel %>" name="clientEdit[tel]"  >
                        </div>
                        <div class="form-group mx-2">
                            <!-- Sign up button -->
                            <button class="btn btn-info btn-block" type="submit">Editar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      
    </body>
</html>
