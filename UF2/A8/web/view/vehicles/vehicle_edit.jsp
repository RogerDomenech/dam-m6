<%-- 
    Document   : newjsp
    Created on : 11-mar-2020, 20:07:17
    Author     : Kitsune
--%>


<%@page import="A8.Client"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*,A8.Vehicle"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="included/css/bootstrap.css">
        <script src="included/js/bootstrap.js"></script>
    </head>
    <body>
        <div class="content ml-2 mr-2 col">
            <% @SuppressWarnings(  "unchecked")
                List<Vehicle> edit_vehicle = (List<Vehicle>) request.getAttribute("edit_vehicle");
                String matricula = edit_vehicle.get(0).getMatricula();
                String marca = edit_vehicle.get(0).getMarca();
                String model = edit_vehicle.get(0).getModel();
                Integer portes = edit_vehicle.get(0).getNumPortes();
                String carburant = edit_vehicle.get(0).getCarburant();
                Date ultVisita = edit_vehicle.get(0).getUltVisita();
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                String printdate = formatDate.format(ultVisita);
                Boolean vehicleEmpresa = edit_vehicle.get(0).getVehileEmpresa();
                Integer kms = edit_vehicle.get(0).getKm();
            %>
            <div class ="row">
                <h1>Edita Vehicle</h1>
            </div>
            <div class="row">
                <div class="col-10">
                    <form class="border border-light" action="A8">
                        <input type="hidden" name="vehicleEdit[matricula]" value="<%= matricula%>">
                        <div class="row">
                            <div class="form-group">    
                                <p class="h4 mb-4">Edita Vehicle: <%= matricula%></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col mx-3>" 
                                 <div class="form-group">
                                    <div class ="form-inline">
                                        <label class="col-sm-2 col-form-label">Model:</label>
                                        <input type="text" class="my-1 form-control" placeholder="<%= model%>" name="vehicleEdit[model]">
                                        <label class="col-sm-2 col-form-label">Marca:</label>
                                        <input type="text" class="my-1 form-control" placeholder="<%= marca%>" name="vehicleEdit[marca]">  
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col mx-3">
                                    <div class="form-group">
                                        <div class="form-inline">
                                            <label class="col-sm-2 col-form-label">Numero de portes:</label>
                                            <input type="number" class="my-1 form-control" placeholder="<%= portes%>" name="vehicleEdit[portes]" min="3" max="5">
                                            <label class="col-sm-2 col-form-label">Carburant: </label>
                                            <select class="my-1 form-control" name="vehicleEdit[carburant]">
                                                <option default><%= carburant%></option>
                                                <option value="diesel">Diesel</option>
                                                <option value="gasolina">Gasolina</option>
                                                <option value="electric">Electric</option>
                                                <option value="hibrid">Hibrid</option>
                                            </select>
                                            <label class="col-sm-2 col-form-label">Kilometres: </label>
                                            <input type="number" class="my-1 form-control" placeholder="<%= kms%>" name="vehicleEdit[kms]" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">           
                                <div class="col mx-3">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label class="col-sm-2 col-form-label">Ultima visita</label>
                                            <input type="date" class="mx-3 form-control" value="<%=printdate%>" name="vehicleEdit[ultVist]" >
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 col-form-label">Vehicle d'Empresa</label>
                                            <div class="mx-3 form-group">
                                                <div class="mx-3 form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="vehicleEdit[esEmpresa]" value="true" <%= (vehicleEmpresa) ? "checked" : ""%>>
                                                    <label class="form-check-label">Si</label>
                                                </div>
                                                <div class="mx-3 form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="vehicleEdit[esEmpresa]" value="false" <%= (!vehicleEmpresa) ? "checked" : ""%>>
                                                    <label class="form-check-label" >No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label class="col-sm-2 col-form-label">Client</label>
                                            <select class="my-1 form-control" name="vehicleEdit[client]">
                                                <option></option>
                                                <%
                                                    @SuppressWarnings(  "unchecked")
                                                    List<Client> clients = (List<Client>) request.getAttribute("clients");
                                                    Client selectclient = (Client)request.getAttribute("client");

                                                    if (clients == null) {
                                                       

                                                %><option>No hi ha clients</option><%                                                        
                                                } else {
                                                %>
                                                <option value="<%= selectclient.getDNI()%>" selected><%= selectclient.getNom()%></option>        
                                                <%
                                                    for (Client client : clients) {
                                                        String nom = client.getNom();
                                                        String DNI = client.getDNI();
                                                %>
                                                <option value="<%=DNI%>"><%=nom%></option>
                                                <%}
                                                    }%>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-3 mx-3">
                                    <button class="btn btn-info btn-block" type="submit">Edit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

    </body>
</html>
