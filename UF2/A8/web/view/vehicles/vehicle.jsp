<%-- 
    Document   : newjsp
    Created on : 11-mar-2020, 20:07:17
    Author     : Kitsune
--%>


<%@page import="A8.Client"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*,A8.Vehicle"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="included/css/bootstrap.css">
        <script src="included/js/bootstrap.js"></script>
    </head>
    <body>
        <nav class="nav">
            <a class="nav-link" href="./">Index</a>
            <a class="nav-link" href="A8?main">Principal</a>
            <a class="nav-link" href="A8?clients">Clients</a>
            <a class="nav-link" active href="A8?vehicles">Vehicles</a>
        </nav>
        <div class="content ml-2 mr-2 col">

            <div class ="row">
                <h1>Vehicles</h1>
            </div>
            <div class="row">
                <div class="col-10">
                    <!-- Default form register -->
                    <form class="border border-light" action="A8">

                        <div class="row">
                            <div class="form-group">    
                                <p class="h4 mb-4">Nou Vehicle</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 mx-3>" 
                                 <div class="form-group">
                                    <input type="text" class="my-1 form-control" placeholder="Matricula del Vehicle" name="vehicle[matricula]" required>
                                    <input type="text" class="my-1 form-control" placeholder="Model" name="vehicle[model]" required>
                                    <input type="text" class="my-1 form-control" placeholder="Marca" name="vehicle[marca]" required>  
                                </div>

                                <div class="col-2 mx-3">
                                    <div class="form-group">
                                        <input type="number" class="my-1 form-control" placeholder="Numero de portes" name="vehicle[portes]" min="3" max="5" required>
                                        <select class="my-1 form-control" name="vehicle[carburant]">
                                            <option value="diesel" default>Diesel</option>
                                            <option value="gasolina">Gasolina</option>
                                            <option value="electric">Electric</option>
                                            <option value="hibrid">Hibrid</option>
                                        </select>
                                        <input type="number" class="my-1 form-control" placeholder="Numero de Kilometres" name="vehicle[kms]" required>
                                    </div>
                                </div>
                                <div class="col-2 mx-3">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label class="col-sm-2 col-form-label">Ultima visita</label>
                                            <input type="date" class="mx-3 form-control" placeholder="Data de l'Ultima visita" name="vehicle[ultVist]" required>
                                        </div>
                                    </div>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label class="col-sm-2 col-form-label">Vehicle d'Empresa</label>
                                            <div class="mx-3 form-group">
                                                <div class="mx-3 form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="vehicle[esEmpresa]" value="true">
                                                    <label class="form-check-label" >Si</label>
                                                </div>
                                                <div class="mx-3 form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="vehicle[esEmpresa]" value="false">
                                                    <label class="form-check-label" >No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label class="col-sm-2 col-form-label">Client</label>
                                            <select class="my-1 form-control" name="vehicle[client]">
                                                <option></option>
                                                <%
                                                    @SuppressWarnings(  "unchecked")
                                                    List<Client> clients = (List<Client>) request.getAttribute("clients");

                                                    if (clients == null) {

                                                %><option>No hi ha clients</option><%                                                        } else {
                                                            for (Client client : clients) {
                                                                String nom = client.getNom();
                                                                String DNI = client.getDNI();
                                                %>
                                                <option value="<%=DNI%>"><%=nom%></option>
                                                <%}
                                                                }%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2 mx-3">
                                    <button class="btn btn-info btn-block" type="submit">Add</button>
                                </div>
                            </div>

                    </form>
                </div>
            </div>
            <!-- Default form register -->

            <hr>
            <div class="row">
                <h2>Vehicles</h2>
            </div>

            <div class="row">
                <%
                    @SuppressWarnings(  "unchecked")
                    List<Vehicle> vehicles = (List<Vehicle>) request.getAttribute("vehicles");

                    if (vehicles == null) {
                %><h3> No hi ha Vehicles</h3>
                <%} else {%>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Matricula</th>
                            <th scope="col">Model</th>
                            <th scope="col">Marca</th>
                            <th scope="col">Portes</th>
                            <th scope="col">Carburant</th>
                            <th scope="col">Km</th>
                            <th scope="col">Ultima visita</th>
                            <th scope="col">Vehicle d'empresa</th>
                            <th scope="col">Accions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%for (Vehicle vehicle : vehicles) {
                                String matricula = vehicle.getMatricula();
                                String model = vehicle.getModel();
                                String marca = vehicle.getMarca();
                                Integer numPortes = vehicle.getNumPortes();
                                String carburant = vehicle.getCarburant();
                                Integer km = vehicle.getKm();
                                Date ultVisita = vehicle.getUltVisita();
                                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                                String printdate = formatDate.format(ultVisita);
                                Boolean esEmpresa = vehicle.getVehileEmpresa();
                        %>

                        <tr>
                            <td scope="row"> <%= matricula%> </td>
                            <td> <%= model%> </td>
                            <td> <%= marca%> </td>
                            <td> <%= numPortes%> </td>
                            <td> <%= carburant%> </td>
                            <td> <%= km%> </td>
                            <td> <%= printdate%> </td>
                            <td> <%= (esEmpresa) ? "Si" : "No"%> </td>
                            <td> <a class="mx-1" href="./A8?vedit=<%=matricula%>">Editar</a><a class="mx-1" href="./A8?vdel=<%=matricula%>">Borrar</a> </td>
                        </tr> 
                        <%
                                }
                            }%>
                    </tbody>
                </table>
            </div>
        </div>

    </body>
</html>
