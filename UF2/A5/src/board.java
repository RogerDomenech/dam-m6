
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Kitsune
 */
public class Board extends javax.swing.JFrame {

    //propietats
    //jugadors
    boolean jugaX;
    boolean jugaO;
    //cooredenades origien
    int columnaOrigen = -1;
    int filaOrigen = -1;
    //partida
    db.Partida partida;
    //inici del joc
    boolean gameStart = false;
    //nom columnes
    String[] nameColumms ={
                "A", "B", "C", "D", "E", "F", "I", "J"
            };
    Object [][] board = new Object[8][8];
    /**
     * Creates new form board
     */
    public Board() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        boardTable = new javax.swing.JTable();
        btnNewGame = new javax.swing.JButton();
        btnLoadLast = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        boardTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", null, "", null, "", null, "", null},
                {null, "", null, "", null, "", null, ""},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {"", null, "", null, "", null, "", null},
                {null, "", null, "", null, "", null, ""}
            },
            new String [] {
                "A", "B", "C", "D", "E", "F", "I", "J"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        boardTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boardTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(boardTable);

        btnNewGame.setText("New game");
        btnNewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewGameActionPerformed(evt);
            }
        });

        btnLoadLast.setText("Load Last");
        btnLoadLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadLastActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addComponent(btnNewGame)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLoadLast)
                .addGap(14, 14, 14))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNewGame)
                    .addComponent(btnLoadLast))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void boardTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_boardTableMouseClicked
       if(gameStart){
        //selecciona casella
        int columna = obtenirColumnaClicada();
        int fila = obtenirFilaClicada();
    
       //saveGame();
       try{
        //si noHiHaOrigen()
        if(noHiHaOrigen()&& !esBuit(fila, columna)){
            test("noHiHaOrigen TRUE",fila, columna);
            //si jugaX i EsX(fila,columna) llavors ActualitzaNouOrigen(fila,columna)
            if(jugaX && EsX(fila, columna)){
                ActualitzaNouOrigen(fila, columna);
                test("jugaX",fila, columna);
            }else if(jugaO && EsO(fila, columna)){
              //sino si jugaO i EsO(fila,columna) llavors ActualitzaNouOrigen(fila,columna)
               ActualitzaNouOrigen(fila, columna);
            }else{
              //encas contrari mostraError()
                mostarError();
            }
        }else{
            test("noHiHaOrigin = false",fila, columna);
        //no noHiHaOrigen()
            if(movimentValid(fila, columna)){
                //si movimentValid(fila,columna) llavors 
                //si esBuit(fila,columna) o OcumpatContrari(fila,columna) llavors ActualitzaNouOrigen(fila,columna)
                if(esBuit(fila, columna)|| OcupatContrari(fila, columna)){
                     mou(fila, columna);
                    //ActualitzaNouOrigen(fila, columna);
                 
                    test("mou", fila, columna);
                }
            }else if (OcupatPropi(fila, columna)){
                //sino si OcupatPropi(fila,columan) llavors ActualitzaNouOrigen(fila,columna) i mou(fila,columna)
                    ActualitzaNouOrigen(fila, columna);
                
            }else{
                    mostrarErrorMoviment();
                }
        }   //encas contrari mostraErrorMoviment()
       }catch(Exception e){
           test("errorGENEREL",fila, columna);
           e.printStackTrace();
       }
      }
    }//GEN-LAST:event_boardTableMouseClicked

    private void btnNewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewGameActionPerformed
        newGame();
        Object [][] board = {
                {"X", null, "X", null, "X", null, "X", null},
                {null, "X", null, "X", null, "X", null, "X"},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {"O", null, "O", null, "O", null, "O", null},
                {null, "O", null, "O", null, "O", null, "O"}
            };
         
        boardTable.setModel(new javax.swing.table.DefaultTableModel(
            board , nameColumms));
        
        jugaX=true;
        jugaO=false;
        
        gameStart=true;
    }//GEN-LAST:event_btnNewGameActionPerformed

    private void btnLoadLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadLastActionPerformed
        try {
            getLastGame();
        } catch (InterruptedException ex) {
            Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        gameStart=true;
    }//GEN-LAST:event_btnLoadLastActionPerformed
    private int obtenirFilaClicada() {
        return boardTable.getSelectedRow();
    }

    private int obtenirColumnaClicada() {
        return boardTable.getSelectedColumn();
    }
    
    private boolean noHiHaOrigen() {
        //mira si origen es  -1 -1
        return (filaOrigen==-1 && columnaOrigen==-1); 
    }

    private boolean EsX(int fila, int columna) {
        String contingutCasella = boardTable.getValueAt(fila, columna).toString();
        return (contingutCasella.equalsIgnoreCase("X"));
    }
    private boolean EsO(int fila, int columna) {
        String contingutCasella = boardTable.getValueAt(fila, columna).toString();
        return (contingutCasella.equalsIgnoreCase("O"));
    }

    private void ActualitzaNouOrigen(int fila, int columna) {
        filaOrigen=fila;
        columnaOrigen=columna;
    }

    private void mostarError() {
        //Avis error generic
        System.out.println("Error Generic");
    }

    private boolean movimentValid(int fila, int columna) {
        boolean resposta = false;
        //fila i columna son les coordenades de desti
        //columna nomes pot ser +1 o -1
        if (columna == columnaOrigen + 1 || columna == columnaOrigen - 1) {
            //limitem moviments per jugadors
            if (jugaX) {
                //jugador X nomes pot fer movients +1Columna
                resposta = (fila == filaOrigen + 1) ? true : false;
            } else {
                //jugador O nomes pot fer moviments -1Columna
                resposta = (fila == filaOrigen - 1) ? true : false;
            }
        }
        return resposta;
    }

    private boolean esBuit(int fila, int columna) {
        return (boardTable.getValueAt(fila, columna)==null) ? true : false;
    }
    private boolean OcupatContrari(int fila, int columna){
        if(!esBuit(fila, columna)){
        return !OcupatPropi(fila, columna);
        }else{
        return false;
        }
    }
    private void mou(int fila, int columna) {
        //fila i columna son desti 
        //seleciona jugador
        String player = null;
        player = (jugaX) ? "X" : "O";
        //rescribim desti
        boardTable.setValueAt(player, fila, columna);
        //borrem origen
        boardTable.setValueAt(" ", filaOrigen, columnaOrigen);
        
        //cambia el jugador
        if(jugaX){
            jugaX=false;
            jugaO=true;
        }else{
            jugaX=true;
            jugaO=false;
        }
    
        //reiniciem origen
        filaOrigen=-1;
        columnaOrigen=-1;
        // guardem board
        saveGame();
    }
  
    private boolean OcupatPropi(int fila, int columna){
        String player;
        if(!esBuit(fila, columna)){
        player = (jugaX)?"X":"O";
        return (boardTable.getValueAt(fila, columna).toString().equalsIgnoreCase(player));
        }else{
        return false;
        }
    }
   
    private void mostrarErrorMoviment(){
        //avis de moviment erroni
        System.out.println("moviment no valid");
    }
    private void test(String txt,int fila, int columna){
        System.out.println("\nPUNT DE TEST ="+txt+
                           "\nnoHiHaOrigen="+noHiHaOrigen()+
                           "\njugaX="+jugaX+
                           "\njugaO="+jugaO+
                           "\nfilaOrigen="+filaOrigen+
                           "\ncolumnaOrigen="+columnaOrigen+
                           "\nfila="+fila+
                           "\ncolumna="+columna+
                           "\nmovimentValid="+movimentValid(fila, columna)+
                           "\nesBuit="+esBuit(fila, columna)+
                           "\nOcupatContrari="+OcupatContrari(fila, columna)+
                           "\nOcupatPropi="+OcupatPropi(fila, columna));
    }
    //DB
    private void newGame(){
        db.Partida novaPartida = new db.Partida();
        novaPartida.setDate(new Date());
        Session session= HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(novaPartida);
        session.getTransaction().commit();
        session.close();
        partida= novaPartida;
    }
    private void saveGame(){
        // recuperem les dades del board
        //String que contindra les dades
        String dades= "";
        // numero de columnes
        int numColumnes = boardTable.getColumnCount();
        //numero de files
        int numFiles = boardTable.getRowCount();
        // recorrem el taulell
        //files
        for (int numfila = 0; numfila < numFiles; numfila++) {
            //columa
            for (int numcolumna = 0; numcolumna < numColumnes; numcolumna++) {
               if(boardTable.getValueAt(numfila, numcolumna)!=null){
                String contingutCasella = boardTable.getValueAt(numfila, numcolumna).toString();
                dades+=contingutCasella;
               }else{
               dades+=" ";
               }
            }
        }
       // dades = dades.substring(0, dades.length()-1);
        String player=(jugaX)?"X":"O";
        //preparem registre en moviments
        db.Moviments newmoviement = new db.Moviments(partida,player, dades);
        Session session= HibernateUtil.getSessionFactory().openSession();
        //Modifiquem la partida
         //consulta db del nombre moviments actual
        Query queryMovimments =session.createQuery("select count(*) from Moviments where(id_partida = "+partida.getId()+")");
      
        List resultQuery = queryMovimments.list();
        
        int countMoviments = ((Long)resultQuery.get(0)).intValue();
        //modifquem partida
        partida.setMoviments(countMoviments);
        //guardem les dades
        
        session.beginTransaction();
        session.save(newmoviement);
        session.update(partida);
        session.getTransaction().commit();
        session.close();
    }
    private void getLastGame() throws InterruptedException, InvocationTargetException{
        //inici seccio
        Session session= HibernateUtil.getSessionFactory().openSession();
        //query partida
        Query queryPartida = session.createQuery("from Partida order by id desc");
        List <db.Partida> resultPartida = queryPartida.list();
        //recuperem la ultima partida
        //assignem a la partida actual la recuperada
        partida = resultPartida.get(0);
        // query moviments
       //Query queryMovimments =session.createQuery("from Moviments where id_partida like "+partida.getId()+" order by id desc");
        Query queryMovimments =session.createQuery("from Moviments where id_partida like "+partida.getId()+" order by id asc");
       
        List <db.Moviments> resultMoviments = queryMovimments.list();
        //recuperem els ultim moviments
        //db.Moviments moviments = resultMoviments.get(0);
        db.Moviments moviments = new db.Moviments();
        for (int k = 0; k < resultMoviments.size(); k++) {
        
        moviments = resultMoviments.get(k);
        //recuperem el boad
        String lastBoard = moviments.getBoard();
        //passem a array de char el lastBoard
        char[] elementBoard = lastBoard.toCharArray();
        
        //contador de elementBoard
        int count =0;
        //ompli el contingut del elementBoard
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j]=elementBoard[count];
                if((elementBoard.length-1)>count){
                count++;
                }
            }
        }
        
        //imprimim el board
        System.out.println("print Board\n"+lastBoard);   
        boardTable.setModel(new DefaultTableModel(board , nameColumms));
        Thread.sleep(500);
        boardTable.repaint();
        Board.super.repaint();
        //this.repaint();
        //esperem 500ms
       /*
        Thread load = new Thread(new Runnable(){
            public void run(){
            boardTable.setModel(new DefaultTableModel(board , nameColumms));
            boardTable.repaint();
            }
        });
        SwingUtilities.invokeAndWait(()->{
            try {
                load.start();
                Thread.sleep(500);
                Thread.interrupted();
            } catch (InterruptedException ex) {
                Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        */
       /* SwingUtilities.invokeAndWait(new Runnable(){
            public void run(){
                 boardTable.setModel(new DefaultTableModel(board , nameColumms));
            }
        });*/
//        SwingUtilities.invokeAndWait(() -> {
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(board.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        });
        //borrem el board
        //board=new Object[8][8];
        }
        //recuperem jugador
        String player = moviments.getPlayer();
        if(player.equalsIgnoreCase("X")){
            jugaX= false;
            jugaO= true;
        }else{
            jugaO= false;
            jugaX= true;
        }
        session.close();
        
    }
    private void loadTable(){
        boardTable.setModel(new DefaultTableModel(board , nameColumms));
       
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Board().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable boardTable;
    private javax.swing.JButton btnLoadLast;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
